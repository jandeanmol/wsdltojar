
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LiftStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LiftStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="CheckedIn"/>
 *     &lt;enumeration value="Boarded"/>
 *     &lt;enumeration value="NoShow"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LiftStatus", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum LiftStatus {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("CheckedIn")
    CHECKED_IN("CheckedIn"),
    @XmlEnumValue("Boarded")
    BOARDED("Boarded"),
    @XmlEnumValue("NoShow")
    NO_SHOW("NoShow"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    LiftStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LiftStatus fromValue(String v) {
        for (LiftStatus c: LiftStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
