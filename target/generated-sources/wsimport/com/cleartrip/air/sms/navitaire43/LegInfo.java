
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for LegInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="AdjustedCapacity" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="EquipmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTypeSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrvLTV" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="Capacity" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="CodeShareIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureTerminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeptLTV" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ETicket" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="FlifoUpdated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IROP" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}LegStatus" minOccurs="0"/>
 *         &lt;element name="Lid" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="OnTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxSTA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PaxSTD" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="PRBCCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScheduleServiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sold" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="OutMoveDays" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="BackMoveDays" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="LegNests" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfLegNest" minOccurs="0"/>
 *         &lt;element name="LegSSRs" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfLegSSR" minOccurs="0"/>
 *         &lt;element name="OperatingFlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperatedByText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperatingCarrier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperatingOpSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubjectToGovtApproval" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarketingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangeOfDirection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MarketingOverride" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AircraftOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegInfo", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "adjustedCapacity",
    "equipmentType",
    "equipmentTypeSuffix",
    "arrivalTerminal",
    "arrvLTV",
    "capacity",
    "codeShareIndicator",
    "departureTerminal",
    "deptLTV",
    "eTicket",
    "flifoUpdated",
    "irop",
    "status",
    "lid",
    "onTime",
    "paxSTA",
    "paxSTD",
    "prbcCode",
    "scheduleServiceType",
    "sold",
    "outMoveDays",
    "backMoveDays",
    "legNests",
    "legSSRs",
    "operatingFlightNumber",
    "operatedByText",
    "operatingCarrier",
    "operatingOpSuffix",
    "subjectToGovtApproval",
    "marketingCode",
    "changeOfDirection",
    "marketingOverride",
    "aircraftOwner"
})
public class LegInfo
    extends StateMessage
{

    @XmlElement(name = "AdjustedCapacity")
    protected Short adjustedCapacity;
    @XmlElementRef(name = "EquipmentType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> equipmentType;
    @XmlElementRef(name = "EquipmentTypeSuffix", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> equipmentTypeSuffix;
    @XmlElementRef(name = "ArrivalTerminal", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalTerminal;
    @XmlElement(name = "ArrvLTV")
    protected Short arrvLTV;
    @XmlElement(name = "Capacity")
    protected Short capacity;
    @XmlElementRef(name = "CodeShareIndicator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codeShareIndicator;
    @XmlElementRef(name = "DepartureTerminal", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureTerminal;
    @XmlElement(name = "DeptLTV")
    protected Short deptLTV;
    @XmlElement(name = "ETicket")
    protected Boolean eTicket;
    @XmlElement(name = "FlifoUpdated")
    protected Boolean flifoUpdated;
    @XmlElement(name = "IROP")
    protected Boolean irop;
    @XmlElement(name = "Status")
    protected LegStatus status;
    @XmlElement(name = "Lid")
    protected Short lid;
    @XmlElementRef(name = "OnTime", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> onTime;
    @XmlElement(name = "PaxSTA")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paxSTA;
    @XmlElement(name = "PaxSTD")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar paxSTD;
    @XmlElementRef(name = "PRBCCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prbcCode;
    @XmlElementRef(name = "ScheduleServiceType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> scheduleServiceType;
    @XmlElement(name = "Sold")
    protected Short sold;
    @XmlElement(name = "OutMoveDays")
    protected Short outMoveDays;
    @XmlElement(name = "BackMoveDays")
    protected Short backMoveDays;
    @XmlElementRef(name = "LegNests", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLegNest> legNests;
    @XmlElementRef(name = "LegSSRs", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLegSSR> legSSRs;
    @XmlElementRef(name = "OperatingFlightNumber", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> operatingFlightNumber;
    @XmlElementRef(name = "OperatedByText", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> operatedByText;
    @XmlElementRef(name = "OperatingCarrier", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> operatingCarrier;
    @XmlElementRef(name = "OperatingOpSuffix", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> operatingOpSuffix;
    @XmlElement(name = "SubjectToGovtApproval")
    protected Boolean subjectToGovtApproval;
    @XmlElementRef(name = "MarketingCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> marketingCode;
    @XmlElement(name = "ChangeOfDirection")
    protected Boolean changeOfDirection;
    @XmlElement(name = "MarketingOverride")
    protected Boolean marketingOverride;
    @XmlElementRef(name = "AircraftOwner", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> aircraftOwner;

    /**
     * Gets the value of the adjustedCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAdjustedCapacity() {
        return adjustedCapacity;
    }

    /**
     * Sets the value of the adjustedCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAdjustedCapacity(Short value) {
        this.adjustedCapacity = value;
    }

    /**
     * Gets the value of the equipmentType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEquipmentType() {
        return equipmentType;
    }

    /**
     * Sets the value of the equipmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEquipmentType(JAXBElement<String> value) {
        this.equipmentType = value;
    }

    /**
     * Gets the value of the equipmentTypeSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEquipmentTypeSuffix() {
        return equipmentTypeSuffix;
    }

    /**
     * Sets the value of the equipmentTypeSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEquipmentTypeSuffix(JAXBElement<String> value) {
        this.equipmentTypeSuffix = value;
    }

    /**
     * Gets the value of the arrivalTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalTerminal() {
        return arrivalTerminal;
    }

    /**
     * Sets the value of the arrivalTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalTerminal(JAXBElement<String> value) {
        this.arrivalTerminal = value;
    }

    /**
     * Gets the value of the arrvLTV property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getArrvLTV() {
        return arrvLTV;
    }

    /**
     * Sets the value of the arrvLTV property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setArrvLTV(Short value) {
        this.arrvLTV = value;
    }

    /**
     * Gets the value of the capacity property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCapacity() {
        return capacity;
    }

    /**
     * Sets the value of the capacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCapacity(Short value) {
        this.capacity = value;
    }

    /**
     * Gets the value of the codeShareIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodeShareIndicator() {
        return codeShareIndicator;
    }

    /**
     * Sets the value of the codeShareIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodeShareIndicator(JAXBElement<String> value) {
        this.codeShareIndicator = value;
    }

    /**
     * Gets the value of the departureTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureTerminal() {
        return departureTerminal;
    }

    /**
     * Sets the value of the departureTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureTerminal(JAXBElement<String> value) {
        this.departureTerminal = value;
    }

    /**
     * Gets the value of the deptLTV property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getDeptLTV() {
        return deptLTV;
    }

    /**
     * Sets the value of the deptLTV property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setDeptLTV(Short value) {
        this.deptLTV = value;
    }

    /**
     * Gets the value of the eTicket property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isETicket() {
        return eTicket;
    }

    /**
     * Sets the value of the eTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setETicket(Boolean value) {
        this.eTicket = value;
    }

    /**
     * Gets the value of the flifoUpdated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFlifoUpdated() {
        return flifoUpdated;
    }

    /**
     * Sets the value of the flifoUpdated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFlifoUpdated(Boolean value) {
        this.flifoUpdated = value;
    }

    /**
     * Gets the value of the irop property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIROP() {
        return irop;
    }

    /**
     * Sets the value of the irop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIROP(Boolean value) {
        this.irop = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link LegStatus }
     *     
     */
    public LegStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link LegStatus }
     *     
     */
    public void setStatus(LegStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the lid property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getLid() {
        return lid;
    }

    /**
     * Sets the value of the lid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setLid(Short value) {
        this.lid = value;
    }

    /**
     * Gets the value of the onTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOnTime() {
        return onTime;
    }

    /**
     * Sets the value of the onTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOnTime(JAXBElement<String> value) {
        this.onTime = value;
    }

    /**
     * Gets the value of the paxSTA property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaxSTA() {
        return paxSTA;
    }

    /**
     * Sets the value of the paxSTA property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaxSTA(XMLGregorianCalendar value) {
        this.paxSTA = value;
    }

    /**
     * Gets the value of the paxSTD property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaxSTD() {
        return paxSTD;
    }

    /**
     * Sets the value of the paxSTD property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaxSTD(XMLGregorianCalendar value) {
        this.paxSTD = value;
    }

    /**
     * Gets the value of the prbcCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPRBCCode() {
        return prbcCode;
    }

    /**
     * Sets the value of the prbcCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPRBCCode(JAXBElement<String> value) {
        this.prbcCode = value;
    }

    /**
     * Gets the value of the scheduleServiceType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getScheduleServiceType() {
        return scheduleServiceType;
    }

    /**
     * Sets the value of the scheduleServiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setScheduleServiceType(JAXBElement<String> value) {
        this.scheduleServiceType = value;
    }

    /**
     * Gets the value of the sold property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSold() {
        return sold;
    }

    /**
     * Sets the value of the sold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSold(Short value) {
        this.sold = value;
    }

    /**
     * Gets the value of the outMoveDays property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getOutMoveDays() {
        return outMoveDays;
    }

    /**
     * Sets the value of the outMoveDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setOutMoveDays(Short value) {
        this.outMoveDays = value;
    }

    /**
     * Gets the value of the backMoveDays property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getBackMoveDays() {
        return backMoveDays;
    }

    /**
     * Sets the value of the backMoveDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setBackMoveDays(Short value) {
        this.backMoveDays = value;
    }

    /**
     * Gets the value of the legNests property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLegNest }{@code >}
     *     
     */
    public JAXBElement<ArrayOfLegNest> getLegNests() {
        return legNests;
    }

    /**
     * Sets the value of the legNests property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLegNest }{@code >}
     *     
     */
    public void setLegNests(JAXBElement<ArrayOfLegNest> value) {
        this.legNests = value;
    }

    /**
     * Gets the value of the legSSRs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLegSSR }{@code >}
     *     
     */
    public JAXBElement<ArrayOfLegSSR> getLegSSRs() {
        return legSSRs;
    }

    /**
     * Sets the value of the legSSRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLegSSR }{@code >}
     *     
     */
    public void setLegSSRs(JAXBElement<ArrayOfLegSSR> value) {
        this.legSSRs = value;
    }

    /**
     * Gets the value of the operatingFlightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOperatingFlightNumber() {
        return operatingFlightNumber;
    }

    /**
     * Sets the value of the operatingFlightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOperatingFlightNumber(JAXBElement<String> value) {
        this.operatingFlightNumber = value;
    }

    /**
     * Gets the value of the operatedByText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOperatedByText() {
        return operatedByText;
    }

    /**
     * Sets the value of the operatedByText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOperatedByText(JAXBElement<String> value) {
        this.operatedByText = value;
    }

    /**
     * Gets the value of the operatingCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOperatingCarrier() {
        return operatingCarrier;
    }

    /**
     * Sets the value of the operatingCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOperatingCarrier(JAXBElement<String> value) {
        this.operatingCarrier = value;
    }

    /**
     * Gets the value of the operatingOpSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOperatingOpSuffix() {
        return operatingOpSuffix;
    }

    /**
     * Sets the value of the operatingOpSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOperatingOpSuffix(JAXBElement<String> value) {
        this.operatingOpSuffix = value;
    }

    /**
     * Gets the value of the subjectToGovtApproval property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSubjectToGovtApproval() {
        return subjectToGovtApproval;
    }

    /**
     * Sets the value of the subjectToGovtApproval property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSubjectToGovtApproval(Boolean value) {
        this.subjectToGovtApproval = value;
    }

    /**
     * Gets the value of the marketingCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMarketingCode() {
        return marketingCode;
    }

    /**
     * Sets the value of the marketingCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMarketingCode(JAXBElement<String> value) {
        this.marketingCode = value;
    }

    /**
     * Gets the value of the changeOfDirection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChangeOfDirection() {
        return changeOfDirection;
    }

    /**
     * Sets the value of the changeOfDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChangeOfDirection(Boolean value) {
        this.changeOfDirection = value;
    }

    /**
     * Gets the value of the marketingOverride property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMarketingOverride() {
        return marketingOverride;
    }

    /**
     * Sets the value of the marketingOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMarketingOverride(Boolean value) {
        this.marketingOverride = value;
    }

    /**
     * Gets the value of the aircraftOwner property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAircraftOwner() {
        return aircraftOwner;
    }

    /**
     * Sets the value of the aircraftOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAircraftOwner(JAXBElement<String> value) {
        this.aircraftOwner = value;
    }

}
