
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegSSR complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegSSR">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SSRNestCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSRLid" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="SSRSold" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="SSRValueSold" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegSSR", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "ssrNestCode",
    "ssrLid",
    "ssrSold",
    "ssrValueSold"
})
public class LegSSR {

    @XmlElementRef(name = "SSRNestCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ssrNestCode;
    @XmlElement(name = "SSRLid")
    protected Short ssrLid;
    @XmlElement(name = "SSRSold")
    protected Short ssrSold;
    @XmlElement(name = "SSRValueSold")
    protected Short ssrValueSold;

    /**
     * Gets the value of the ssrNestCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSSRNestCode() {
        return ssrNestCode;
    }

    /**
     * Sets the value of the ssrNestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSSRNestCode(JAXBElement<String> value) {
        this.ssrNestCode = value;
    }

    /**
     * Gets the value of the ssrLid property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSSRLid() {
        return ssrLid;
    }

    /**
     * Sets the value of the ssrLid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSSRLid(Short value) {
        this.ssrLid = value;
    }

    /**
     * Gets the value of the ssrSold property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSSRSold() {
        return ssrSold;
    }

    /**
     * Sets the value of the ssrSold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSSRSold(Short value) {
        this.ssrSold = value;
    }

    /**
     * Gets the value of the ssrValueSold property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSSRValueSold() {
        return ssrValueSold;
    }

    /**
     * Sets the value of the ssrValueSold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSSRValueSold(Short value) {
        this.ssrValueSold = value;
    }

}
