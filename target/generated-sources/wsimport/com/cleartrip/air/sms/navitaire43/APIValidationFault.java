
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for APIValidationFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="APIValidationFault">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/FaultContracts}APIFault">
 *       &lt;sequence>
 *         &lt;element name="ValidationResults" type="{http://www.navitaire.com/Ncl/Validation/ValidationResult}ArrayOfValidationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "APIValidationFault", namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", propOrder = {
    "validationResults"
})
public class APIValidationFault
    extends APIFault
{

    @XmlElementRef(name = "ValidationResults", namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfValidationResult> validationResults;

    /**
     * Gets the value of the validationResults property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfValidationResult }{@code >}
     *     
     */
    public JAXBElement<ArrayOfValidationResult> getValidationResults() {
        return validationResults;
    }

    /**
     * Sets the value of the validationResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfValidationResult }{@code >}
     *     
     */
    public void setValidationResults(JAXBElement<ArrayOfValidationResult> value) {
        this.validationResults = value;
    }

}
