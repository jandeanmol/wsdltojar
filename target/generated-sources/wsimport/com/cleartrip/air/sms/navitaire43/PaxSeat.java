
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaxSeat complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaxSeat">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="PassengerNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ArrivalStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitDesignator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompartmentDesignator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SeatPreference" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}SeatPreference" minOccurs="0"/>
 *         &lt;element name="Penalty" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SeatTogetherPreference" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PaxSeatInfo" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}PaxSeatInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxSeat", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "passengerNumber",
    "arrivalStation",
    "departureStation",
    "unitDesignator",
    "compartmentDesignator",
    "seatPreference",
    "penalty",
    "seatTogetherPreference",
    "paxSeatInfo"
})
public class PaxSeat
    extends StateMessage
{

    @XmlElement(name = "PassengerNumber")
    protected Short passengerNumber;
    @XmlElementRef(name = "ArrivalStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalStation;
    @XmlElementRef(name = "DepartureStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureStation;
    @XmlElementRef(name = "UnitDesignator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unitDesignator;
    @XmlElementRef(name = "CompartmentDesignator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compartmentDesignator;
    @XmlElement(name = "SeatPreference")
    protected SeatPreference seatPreference;
    @XmlElement(name = "Penalty")
    protected Integer penalty;
    @XmlElement(name = "SeatTogetherPreference")
    protected Boolean seatTogetherPreference;
    @XmlElementRef(name = "PaxSeatInfo", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<PaxSeatInfo> paxSeatInfo;

    /**
     * Gets the value of the passengerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPassengerNumber() {
        return passengerNumber;
    }

    /**
     * Sets the value of the passengerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPassengerNumber(Short value) {
        this.passengerNumber = value;
    }

    /**
     * Gets the value of the arrivalStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalStation() {
        return arrivalStation;
    }

    /**
     * Sets the value of the arrivalStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalStation(JAXBElement<String> value) {
        this.arrivalStation = value;
    }

    /**
     * Gets the value of the departureStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureStation() {
        return departureStation;
    }

    /**
     * Sets the value of the departureStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureStation(JAXBElement<String> value) {
        this.departureStation = value;
    }

    /**
     * Gets the value of the unitDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnitDesignator() {
        return unitDesignator;
    }

    /**
     * Sets the value of the unitDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnitDesignator(JAXBElement<String> value) {
        this.unitDesignator = value;
    }

    /**
     * Gets the value of the compartmentDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompartmentDesignator() {
        return compartmentDesignator;
    }

    /**
     * Sets the value of the compartmentDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompartmentDesignator(JAXBElement<String> value) {
        this.compartmentDesignator = value;
    }

    /**
     * Gets the value of the seatPreference property.
     * 
     * @return
     *     possible object is
     *     {@link SeatPreference }
     *     
     */
    public SeatPreference getSeatPreference() {
        return seatPreference;
    }

    /**
     * Sets the value of the seatPreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatPreference }
     *     
     */
    public void setSeatPreference(SeatPreference value) {
        this.seatPreference = value;
    }

    /**
     * Gets the value of the penalty property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPenalty() {
        return penalty;
    }

    /**
     * Sets the value of the penalty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPenalty(Integer value) {
        this.penalty = value;
    }

    /**
     * Gets the value of the seatTogetherPreference property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSeatTogetherPreference() {
        return seatTogetherPreference;
    }

    /**
     * Sets the value of the seatTogetherPreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSeatTogetherPreference(Boolean value) {
        this.seatTogetherPreference = value;
    }

    /**
     * Gets the value of the paxSeatInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PaxSeatInfo }{@code >}
     *     
     */
    public JAXBElement<PaxSeatInfo> getPaxSeatInfo() {
        return paxSeatInfo;
    }

    /**
     * Sets the value of the paxSeatInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PaxSeatInfo }{@code >}
     *     
     */
    public void setPaxSeatInfo(JAXBElement<PaxSeatInfo> value) {
        this.paxSeatInfo = value;
    }

}
