
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContentDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContentDataType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="Text"/>
 *     &lt;enumeration value="RichTextFormat"/>
 *     &lt;enumeration value="ImageJPG"/>
 *     &lt;enumeration value="ImageGIF"/>
 *     &lt;enumeration value="ImageBMP"/>
 *     &lt;enumeration value="ImagePNG"/>
 *     &lt;enumeration value="WordDoc"/>
 *     &lt;enumeration value="HTML"/>
 *     &lt;enumeration value="PDF"/>
 *     &lt;enumeration value="URI"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContentDataType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum ContentDataType {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Text")
    TEXT("Text"),
    @XmlEnumValue("RichTextFormat")
    RICH_TEXT_FORMAT("RichTextFormat"),
    @XmlEnumValue("ImageJPG")
    IMAGE_JPG("ImageJPG"),
    @XmlEnumValue("ImageGIF")
    IMAGE_GIF("ImageGIF"),
    @XmlEnumValue("ImageBMP")
    IMAGE_BMP("ImageBMP"),
    @XmlEnumValue("ImagePNG")
    IMAGE_PNG("ImagePNG"),
    @XmlEnumValue("WordDoc")
    WORD_DOC("WordDoc"),
    HTML("HTML"),
    PDF("PDF"),
    URI("URI"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    ContentDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContentDataType fromValue(String v) {
        for (ContentDataType c: ContentDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
