
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.navitaire.com/WebServices/DataContracts/Content}Cat50FareRuleInfoRequestData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cat50FareRuleInfoRequestData"
})
@XmlRootElement(name = "GetCat50FareRuleInfoRequest", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/ContentService")
public class GetCat50FareRuleInfoRequest {

    @XmlElement(name = "Cat50FareRuleInfoRequestData", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", nillable = true)
    protected Cat50FareRuleInfoRequest cat50FareRuleInfoRequestData;

    /**
     * Gets the value of the cat50FareRuleInfoRequestData property.
     * 
     * @return
     *     possible object is
     *     {@link Cat50FareRuleInfoRequest }
     *     
     */
    public Cat50FareRuleInfoRequest getCat50FareRuleInfoRequestData() {
        return cat50FareRuleInfoRequestData;
    }

    /**
     * Sets the value of the cat50FareRuleInfoRequestData property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cat50FareRuleInfoRequest }
     *     
     */
    public void setCat50FareRuleInfoRequestData(Cat50FareRuleInfoRequest value) {
        this.cat50FareRuleInfoRequestData = value;
    }

}
