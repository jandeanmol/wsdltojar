
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaxScore complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaxScore">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Score" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PassengerNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ActionStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GuestValueCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxScore", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "score",
    "passengerNumber",
    "actionStatusCode",
    "guestValueCode"
})
public class PaxScore {

    @XmlElement(name = "Score")
    protected Integer score;
    @XmlElement(name = "PassengerNumber")
    protected Short passengerNumber;
    @XmlElementRef(name = "ActionStatusCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actionStatusCode;
    @XmlElementRef(name = "GuestValueCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> guestValueCode;

    /**
     * Gets the value of the score property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setScore(Integer value) {
        this.score = value;
    }

    /**
     * Gets the value of the passengerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPassengerNumber() {
        return passengerNumber;
    }

    /**
     * Sets the value of the passengerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPassengerNumber(Short value) {
        this.passengerNumber = value;
    }

    /**
     * Gets the value of the actionStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActionStatusCode() {
        return actionStatusCode;
    }

    /**
     * Sets the value of the actionStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActionStatusCode(JAXBElement<String> value) {
        this.actionStatusCode = value;
    }

    /**
     * Gets the value of the guestValueCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGuestValueCode() {
        return guestValueCode;
    }

    /**
     * Sets the value of the guestValueCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGuestValueCode(JAXBElement<String> value) {
        this.guestValueCode = value;
    }

}
