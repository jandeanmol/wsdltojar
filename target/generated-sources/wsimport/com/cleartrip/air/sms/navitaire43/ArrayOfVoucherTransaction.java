
package com.cleartrip.air.sms.navitaire43;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfVoucherTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfVoucherTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoucherTransaction" type="{http://schemas.navitaire.com/WebServices/DataContracts/Voucher}VoucherTransaction" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfVoucherTransaction", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", propOrder = {
    "voucherTransaction"
})
public class ArrayOfVoucherTransaction {

    @XmlElement(name = "VoucherTransaction", nillable = true)
    protected List<VoucherTransaction> voucherTransaction;

    /**
     * Gets the value of the voucherTransaction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherTransaction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherTransaction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoucherTransaction }
     * 
     * 
     */
    public List<VoucherTransaction> getVoucherTransaction() {
        if (voucherTransaction == null) {
            voucherTransaction = new ArrayList<VoucherTransaction>();
        }
        return this.voucherTransaction;
    }

}
