
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegClass complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegClass">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="ClassNest" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}ClassStatus" minOccurs="0"/>
 *         &lt;element name="ClassType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassAllotted" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ClassRank" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ClassAU" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ClassSold" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="NonStopSold" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ThruSold" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="CnxSold" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="LatestAdvRes" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegClass", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "classNest",
    "classOfService",
    "status",
    "classType",
    "classAllotted",
    "classRank",
    "classAU",
    "classSold",
    "nonStopSold",
    "thruSold",
    "cnxSold",
    "latestAdvRes"
})
public class LegClass
    extends StateMessage
{

    @XmlElement(name = "ClassNest")
    protected Short classNest;
    @XmlElementRef(name = "ClassOfService", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> classOfService;
    @XmlElement(name = "Status")
    protected ClassStatus status;
    @XmlElementRef(name = "ClassType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> classType;
    @XmlElement(name = "ClassAllotted")
    protected Short classAllotted;
    @XmlElement(name = "ClassRank")
    protected Short classRank;
    @XmlElement(name = "ClassAU")
    protected Short classAU;
    @XmlElement(name = "ClassSold")
    protected Short classSold;
    @XmlElement(name = "NonStopSold")
    protected Short nonStopSold;
    @XmlElement(name = "ThruSold")
    protected Short thruSold;
    @XmlElement(name = "CnxSold")
    protected Short cnxSold;
    @XmlElement(name = "LatestAdvRes")
    protected Short latestAdvRes;

    /**
     * Gets the value of the classNest property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getClassNest() {
        return classNest;
    }

    /**
     * Sets the value of the classNest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setClassNest(Short value) {
        this.classNest = value;
    }

    /**
     * Gets the value of the classOfService property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClassOfService() {
        return classOfService;
    }

    /**
     * Sets the value of the classOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClassOfService(JAXBElement<String> value) {
        this.classOfService = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ClassStatus }
     *     
     */
    public ClassStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassStatus }
     *     
     */
    public void setStatus(ClassStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the classType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClassType() {
        return classType;
    }

    /**
     * Sets the value of the classType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClassType(JAXBElement<String> value) {
        this.classType = value;
    }

    /**
     * Gets the value of the classAllotted property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getClassAllotted() {
        return classAllotted;
    }

    /**
     * Sets the value of the classAllotted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setClassAllotted(Short value) {
        this.classAllotted = value;
    }

    /**
     * Gets the value of the classRank property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getClassRank() {
        return classRank;
    }

    /**
     * Sets the value of the classRank property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setClassRank(Short value) {
        this.classRank = value;
    }

    /**
     * Gets the value of the classAU property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getClassAU() {
        return classAU;
    }

    /**
     * Sets the value of the classAU property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setClassAU(Short value) {
        this.classAU = value;
    }

    /**
     * Gets the value of the classSold property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getClassSold() {
        return classSold;
    }

    /**
     * Sets the value of the classSold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setClassSold(Short value) {
        this.classSold = value;
    }

    /**
     * Gets the value of the nonStopSold property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNonStopSold() {
        return nonStopSold;
    }

    /**
     * Sets the value of the nonStopSold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNonStopSold(Short value) {
        this.nonStopSold = value;
    }

    /**
     * Gets the value of the thruSold property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getThruSold() {
        return thruSold;
    }

    /**
     * Sets the value of the thruSold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setThruSold(Short value) {
        this.thruSold = value;
    }

    /**
     * Gets the value of the cnxSold property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCnxSold() {
        return cnxSold;
    }

    /**
     * Sets the value of the cnxSold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCnxSold(Short value) {
        this.cnxSold = value;
    }

    /**
     * Gets the value of the latestAdvRes property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getLatestAdvRes() {
        return latestAdvRes;
    }

    /**
     * Sets the value of the latestAdvRes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setLatestAdvRes(Short value) {
        this.latestAdvRes = value;
    }

}
