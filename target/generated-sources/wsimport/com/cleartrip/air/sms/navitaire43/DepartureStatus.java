
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DepartureStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DepartureStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="Cancelled"/>
 *     &lt;enumeration value="Boarding"/>
 *     &lt;enumeration value="SeeAgent"/>
 *     &lt;enumeration value="Delayed"/>
 *     &lt;enumeration value="Departed"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DepartureStatus", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum DepartureStatus {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled"),
    @XmlEnumValue("Boarding")
    BOARDING("Boarding"),
    @XmlEnumValue("SeeAgent")
    SEE_AGENT("SeeAgent"),
    @XmlEnumValue("Delayed")
    DELAYED("Delayed"),
    @XmlEnumValue("Departed")
    DEPARTED("Departed"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    DepartureStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DepartureStatus fromValue(String v) {
        for (DepartureStatus c: DepartureStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
