
package com.cleartrip.air.sms.navitaire43;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PaxSegment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaxSegment">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="BoardingSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LiftStatus" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}LiftStatus" minOccurs="0"/>
 *         &lt;element name="OverBookIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PassengerNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="PriorityDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TripType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}TripType" minOccurs="0"/>
 *         &lt;element name="TimeChanged" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="POS" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common}PointOfSale" minOccurs="0"/>
 *         &lt;element name="SourcePOS" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common}PointOfSale" minOccurs="0"/>
 *         &lt;element name="VerifiedTravelDocs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModifiedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ActivityDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="BaggageAllowanceWeight" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="BaggageAllowanceWeightType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}WeightType" minOccurs="0"/>
 *         &lt;element name="BaggageAllowanceUsed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="BoardingPassDetail" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}BoardingPassDetail" minOccurs="0"/>
 *         &lt;element name="ServiceBundleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaggageGroupNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxSegment", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "boardingSequence",
    "createdDate",
    "liftStatus",
    "overBookIndicator",
    "passengerNumber",
    "priorityDate",
    "tripType",
    "timeChanged",
    "pos",
    "sourcePOS",
    "verifiedTravelDocs",
    "modifiedDate",
    "activityDate",
    "baggageAllowanceWeight",
    "baggageAllowanceWeightType",
    "baggageAllowanceUsed",
    "referenceNumber",
    "boardingPassDetail",
    "serviceBundleCode",
    "baggageGroupNumber"
})
public class PaxSegment
    extends StateMessage
{

    @XmlElementRef(name = "BoardingSequence", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> boardingSequence;
    @XmlElement(name = "CreatedDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createdDate;
    @XmlElement(name = "LiftStatus")
    protected LiftStatus liftStatus;
    @XmlElementRef(name = "OverBookIndicator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> overBookIndicator;
    @XmlElement(name = "PassengerNumber")
    protected Short passengerNumber;
    @XmlElement(name = "PriorityDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar priorityDate;
    @XmlList
    @XmlElement(name = "TripType")
    protected List<String> tripType;
    @XmlElement(name = "TimeChanged")
    protected Boolean timeChanged;
    @XmlElementRef(name = "POS", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<PointOfSale> pos;
    @XmlElementRef(name = "SourcePOS", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<PointOfSale> sourcePOS;
    @XmlElementRef(name = "VerifiedTravelDocs", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> verifiedTravelDocs;
    @XmlElement(name = "ModifiedDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modifiedDate;
    @XmlElement(name = "ActivityDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activityDate;
    @XmlElement(name = "BaggageAllowanceWeight")
    protected Short baggageAllowanceWeight;
    @XmlElement(name = "BaggageAllowanceWeightType")
    protected WeightType baggageAllowanceWeightType;
    @XmlElement(name = "BaggageAllowanceUsed")
    protected Boolean baggageAllowanceUsed;
    @XmlElement(name = "ReferenceNumber")
    protected Long referenceNumber;
    @XmlElementRef(name = "BoardingPassDetail", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<BoardingPassDetail> boardingPassDetail;
    @XmlElementRef(name = "ServiceBundleCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceBundleCode;
    @XmlElement(name = "BaggageGroupNumber")
    protected Short baggageGroupNumber;

    /**
     * Gets the value of the boardingSequence property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBoardingSequence() {
        return boardingSequence;
    }

    /**
     * Sets the value of the boardingSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBoardingSequence(JAXBElement<String> value) {
        this.boardingSequence = value;
    }

    /**
     * Gets the value of the createdDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the value of the createdDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreatedDate(XMLGregorianCalendar value) {
        this.createdDate = value;
    }

    /**
     * Gets the value of the liftStatus property.
     * 
     * @return
     *     possible object is
     *     {@link LiftStatus }
     *     
     */
    public LiftStatus getLiftStatus() {
        return liftStatus;
    }

    /**
     * Sets the value of the liftStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link LiftStatus }
     *     
     */
    public void setLiftStatus(LiftStatus value) {
        this.liftStatus = value;
    }

    /**
     * Gets the value of the overBookIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOverBookIndicator() {
        return overBookIndicator;
    }

    /**
     * Sets the value of the overBookIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOverBookIndicator(JAXBElement<String> value) {
        this.overBookIndicator = value;
    }

    /**
     * Gets the value of the passengerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPassengerNumber() {
        return passengerNumber;
    }

    /**
     * Sets the value of the passengerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPassengerNumber(Short value) {
        this.passengerNumber = value;
    }

    /**
     * Gets the value of the priorityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPriorityDate() {
        return priorityDate;
    }

    /**
     * Sets the value of the priorityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPriorityDate(XMLGregorianCalendar value) {
        this.priorityDate = value;
    }

    /**
     * Gets the value of the tripType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tripType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTripType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTripType() {
        if (tripType == null) {
            tripType = new ArrayList<String>();
        }
        return this.tripType;
    }

    /**
     * Gets the value of the timeChanged property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTimeChanged() {
        return timeChanged;
    }

    /**
     * Sets the value of the timeChanged property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTimeChanged(Boolean value) {
        this.timeChanged = value;
    }

    /**
     * Gets the value of the pos property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PointOfSale }{@code >}
     *     
     */
    public JAXBElement<PointOfSale> getPOS() {
        return pos;
    }

    /**
     * Sets the value of the pos property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PointOfSale }{@code >}
     *     
     */
    public void setPOS(JAXBElement<PointOfSale> value) {
        this.pos = value;
    }

    /**
     * Gets the value of the sourcePOS property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PointOfSale }{@code >}
     *     
     */
    public JAXBElement<PointOfSale> getSourcePOS() {
        return sourcePOS;
    }

    /**
     * Sets the value of the sourcePOS property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PointOfSale }{@code >}
     *     
     */
    public void setSourcePOS(JAXBElement<PointOfSale> value) {
        this.sourcePOS = value;
    }

    /**
     * Gets the value of the verifiedTravelDocs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVerifiedTravelDocs() {
        return verifiedTravelDocs;
    }

    /**
     * Sets the value of the verifiedTravelDocs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVerifiedTravelDocs(JAXBElement<String> value) {
        this.verifiedTravelDocs = value;
    }

    /**
     * Gets the value of the modifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModifiedDate() {
        return modifiedDate;
    }

    /**
     * Sets the value of the modifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModifiedDate(XMLGregorianCalendar value) {
        this.modifiedDate = value;
    }

    /**
     * Gets the value of the activityDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivityDate() {
        return activityDate;
    }

    /**
     * Sets the value of the activityDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivityDate(XMLGregorianCalendar value) {
        this.activityDate = value;
    }

    /**
     * Gets the value of the baggageAllowanceWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getBaggageAllowanceWeight() {
        return baggageAllowanceWeight;
    }

    /**
     * Sets the value of the baggageAllowanceWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setBaggageAllowanceWeight(Short value) {
        this.baggageAllowanceWeight = value;
    }

    /**
     * Gets the value of the baggageAllowanceWeightType property.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getBaggageAllowanceWeightType() {
        return baggageAllowanceWeightType;
    }

    /**
     * Sets the value of the baggageAllowanceWeightType property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setBaggageAllowanceWeightType(WeightType value) {
        this.baggageAllowanceWeightType = value;
    }

    /**
     * Gets the value of the baggageAllowanceUsed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBaggageAllowanceUsed() {
        return baggageAllowanceUsed;
    }

    /**
     * Sets the value of the baggageAllowanceUsed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBaggageAllowanceUsed(Boolean value) {
        this.baggageAllowanceUsed = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferenceNumber(Long value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the boardingPassDetail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BoardingPassDetail }{@code >}
     *     
     */
    public JAXBElement<BoardingPassDetail> getBoardingPassDetail() {
        return boardingPassDetail;
    }

    /**
     * Sets the value of the boardingPassDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BoardingPassDetail }{@code >}
     *     
     */
    public void setBoardingPassDetail(JAXBElement<BoardingPassDetail> value) {
        this.boardingPassDetail = value;
    }

    /**
     * Gets the value of the serviceBundleCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceBundleCode() {
        return serviceBundleCode;
    }

    /**
     * Sets the value of the serviceBundleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceBundleCode(JAXBElement<String> value) {
        this.serviceBundleCode = value;
    }

    /**
     * Gets the value of the baggageGroupNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getBaggageGroupNumber() {
        return baggageGroupNumber;
    }

    /**
     * Sets the value of the baggageGroupNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setBaggageGroupNumber(Short value) {
        this.baggageGroupNumber = value;
    }

}
