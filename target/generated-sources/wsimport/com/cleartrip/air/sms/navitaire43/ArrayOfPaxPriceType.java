
package com.cleartrip.air.sms.navitaire43;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfPaxPriceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfPaxPriceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaxPriceType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}PaxPriceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfPaxPriceType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "paxPriceType"
})
public class ArrayOfPaxPriceType {

    @XmlElement(name = "PaxPriceType", nillable = true)
    protected List<PaxPriceType> paxPriceType;

    /**
     * Gets the value of the paxPriceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paxPriceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaxPriceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaxPriceType }
     * 
     * 
     */
    public List<PaxPriceType> getPaxPriceType() {
        if (paxPriceType == null) {
            paxPriceType = new ArrayList<PaxPriceType>();
        }
        return this.paxPriceType;
    }

}
