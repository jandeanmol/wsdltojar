
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Content complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Content">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}ModifiedMessage">
 *       &lt;sequence>
 *         &lt;element name="ContentID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ContentType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}ContentType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Size" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CultureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}ContentDataType" minOccurs="0"/>
 *         &lt;element name="Data" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="CreatedAgentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModifiedAgentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Content", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", propOrder = {
    "contentID",
    "contentType",
    "name",
    "description",
    "size",
    "cultureCode",
    "dataType",
    "data",
    "createdAgentName",
    "modifiedAgentName"
})
public class Content
    extends ModifiedMessage
{

    @XmlElement(name = "ContentID")
    protected Long contentID;
    @XmlElement(name = "ContentType")
    protected ContentType contentType;
    @XmlElementRef(name = "Name", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "Description", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElement(name = "Size")
    protected Long size;
    @XmlElementRef(name = "CultureCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cultureCode;
    @XmlElement(name = "DataType")
    protected ContentDataType dataType;
    @XmlElementRef(name = "Data", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> data;
    @XmlElementRef(name = "CreatedAgentName", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<String> createdAgentName;
    @XmlElementRef(name = "ModifiedAgentName", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<String> modifiedAgentName;

    /**
     * Gets the value of the contentID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContentID() {
        return contentID;
    }

    /**
     * Sets the value of the contentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContentID(Long value) {
        this.contentID = value;
    }

    /**
     * Gets the value of the contentType property.
     * 
     * @return
     *     possible object is
     *     {@link ContentType }
     *     
     */
    public ContentType getContentType() {
        return contentType;
    }

    /**
     * Sets the value of the contentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContentType }
     *     
     */
    public void setContentType(ContentType value) {
        this.contentType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSize(Long value) {
        this.size = value;
    }

    /**
     * Gets the value of the cultureCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCultureCode() {
        return cultureCode;
    }

    /**
     * Sets the value of the cultureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCultureCode(JAXBElement<String> value) {
        this.cultureCode = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link ContentDataType }
     *     
     */
    public ContentDataType getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContentDataType }
     *     
     */
    public void setDataType(ContentDataType value) {
        this.dataType = value;
    }

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setData(JAXBElement<byte[]> value) {
        this.data = value;
    }

    /**
     * Gets the value of the createdAgentName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreatedAgentName() {
        return createdAgentName;
    }

    /**
     * Sets the value of the createdAgentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreatedAgentName(JAXBElement<String> value) {
        this.createdAgentName = value;
    }

    /**
     * Gets the value of the modifiedAgentName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getModifiedAgentName() {
        return modifiedAgentName;
    }

    /**
     * Sets the value of the modifiedAgentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setModifiedAgentName(JAXBElement<String> value) {
        this.modifiedAgentName = value;
    }

}
