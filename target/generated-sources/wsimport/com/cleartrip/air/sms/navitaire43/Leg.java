
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Leg complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Leg">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="ArrivalStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STD" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FlightDesignator" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common}FlightDesignator" minOccurs="0"/>
 *         &lt;element name="LegInfo" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}LegInfo" minOccurs="0"/>
 *         &lt;element name="OperationsInfo" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}OperationsInfo" minOccurs="0"/>
 *         &lt;element name="InventoryLegID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Leg", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "arrivalStation",
    "departureStation",
    "sta",
    "std",
    "flightDesignator",
    "legInfo",
    "operationsInfo",
    "inventoryLegID"
})
public class Leg
    extends StateMessage
{

    @XmlElementRef(name = "ArrivalStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalStation;
    @XmlElementRef(name = "DepartureStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureStation;
    @XmlElement(name = "STA")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sta;
    @XmlElement(name = "STD")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar std;
    @XmlElementRef(name = "FlightDesignator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<FlightDesignator> flightDesignator;
    @XmlElementRef(name = "LegInfo", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<LegInfo> legInfo;
    @XmlElementRef(name = "OperationsInfo", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<OperationsInfo> operationsInfo;
    @XmlElement(name = "InventoryLegID")
    protected Long inventoryLegID;

    /**
     * Gets the value of the arrivalStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalStation() {
        return arrivalStation;
    }

    /**
     * Sets the value of the arrivalStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalStation(JAXBElement<String> value) {
        this.arrivalStation = value;
    }

    /**
     * Gets the value of the departureStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureStation() {
        return departureStation;
    }

    /**
     * Sets the value of the departureStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureStation(JAXBElement<String> value) {
        this.departureStation = value;
    }

    /**
     * Gets the value of the sta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTA() {
        return sta;
    }

    /**
     * Sets the value of the sta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTA(XMLGregorianCalendar value) {
        this.sta = value;
    }

    /**
     * Gets the value of the std property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTD() {
        return std;
    }

    /**
     * Sets the value of the std property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTD(XMLGregorianCalendar value) {
        this.std = value;
    }

    /**
     * Gets the value of the flightDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FlightDesignator }{@code >}
     *     
     */
    public JAXBElement<FlightDesignator> getFlightDesignator() {
        return flightDesignator;
    }

    /**
     * Sets the value of the flightDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FlightDesignator }{@code >}
     *     
     */
    public void setFlightDesignator(JAXBElement<FlightDesignator> value) {
        this.flightDesignator = value;
    }

    /**
     * Gets the value of the legInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LegInfo }{@code >}
     *     
     */
    public JAXBElement<LegInfo> getLegInfo() {
        return legInfo;
    }

    /**
     * Sets the value of the legInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LegInfo }{@code >}
     *     
     */
    public void setLegInfo(JAXBElement<LegInfo> value) {
        this.legInfo = value;
    }

    /**
     * Gets the value of the operationsInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OperationsInfo }{@code >}
     *     
     */
    public JAXBElement<OperationsInfo> getOperationsInfo() {
        return operationsInfo;
    }

    /**
     * Sets the value of the operationsInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OperationsInfo }{@code >}
     *     
     */
    public void setOperationsInfo(JAXBElement<OperationsInfo> value) {
        this.operationsInfo = value;
    }

    /**
     * Gets the value of the inventoryLegID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInventoryLegID() {
        return inventoryLegID;
    }

    /**
     * Sets the value of the inventoryLegID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInventoryLegID(Long value) {
        this.inventoryLegID = value;
    }

}
