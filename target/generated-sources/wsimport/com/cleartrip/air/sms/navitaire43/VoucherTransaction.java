
package com.cleartrip.air.sms.navitaire43;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoucherTransaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoucherTransaction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}ModifiedMessage">
 *       &lt;sequence>
 *         &lt;element name="VoucherID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="VoucherTransactionID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="VoucherTransactionType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}VoucherTransactionType" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ForeignAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ForeignCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherTransaction", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", propOrder = {
    "voucherID",
    "voucherTransactionID",
    "voucherTransactionType",
    "amount",
    "currencyCode",
    "foreignAmount",
    "foreignCurrencyCode",
    "recordLocator"
})
public class VoucherTransaction
    extends ModifiedMessage
{

    @XmlElement(name = "VoucherID")
    protected Long voucherID;
    @XmlElement(name = "VoucherTransactionID")
    protected Long voucherTransactionID;
    @XmlElement(name = "VoucherTransactionType")
    protected VoucherTransactionType voucherTransactionType;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "ForeignAmount")
    protected BigDecimal foreignAmount;
    @XmlElementRef(name = "ForeignCurrencyCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> foreignCurrencyCode;
    @XmlElementRef(name = "RecordLocator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordLocator;

    /**
     * Gets the value of the voucherID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVoucherID() {
        return voucherID;
    }

    /**
     * Sets the value of the voucherID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVoucherID(Long value) {
        this.voucherID = value;
    }

    /**
     * Gets the value of the voucherTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVoucherTransactionID() {
        return voucherTransactionID;
    }

    /**
     * Sets the value of the voucherTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVoucherTransactionID(Long value) {
        this.voucherTransactionID = value;
    }

    /**
     * Gets the value of the voucherTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherTransactionType }
     *     
     */
    public VoucherTransactionType getVoucherTransactionType() {
        return voucherTransactionType;
    }

    /**
     * Sets the value of the voucherTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherTransactionType }
     *     
     */
    public void setVoucherTransactionType(VoucherTransactionType value) {
        this.voucherTransactionType = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the foreignAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getForeignAmount() {
        return foreignAmount;
    }

    /**
     * Sets the value of the foreignAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setForeignAmount(BigDecimal value) {
        this.foreignAmount = value;
    }

    /**
     * Gets the value of the foreignCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForeignCurrencyCode() {
        return foreignCurrencyCode;
    }

    /**
     * Sets the value of the foreignCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForeignCurrencyCode(JAXBElement<String> value) {
        this.foreignCurrencyCode = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordLocator(JAXBElement<String> value) {
        this.recordLocator = value;
    }

}
