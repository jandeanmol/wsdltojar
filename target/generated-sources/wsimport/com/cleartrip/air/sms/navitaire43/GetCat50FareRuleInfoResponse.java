
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.navitaire.com/WebServices/DataContracts/Content}Cat50FareRuleInfoResponseData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cat50FareRuleInfoResponseData"
})
@XmlRootElement(name = "GetCat50FareRuleInfoResponse", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/ContentService")
public class GetCat50FareRuleInfoResponse {

    @XmlElement(name = "Cat50FareRuleInfoResponseData", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", nillable = true)
    protected Cat50FareRuleInfoResponseData cat50FareRuleInfoResponseData;

    /**
     * Gets the value of the cat50FareRuleInfoResponseData property.
     * 
     * @return
     *     possible object is
     *     {@link Cat50FareRuleInfoResponseData }
     *     
     */
    public Cat50FareRuleInfoResponseData getCat50FareRuleInfoResponseData() {
        return cat50FareRuleInfoResponseData;
    }

    /**
     * Sets the value of the cat50FareRuleInfoResponseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cat50FareRuleInfoResponseData }
     *     
     */
    public void setCat50FareRuleInfoResponseData(Cat50FareRuleInfoResponseData value) {
        this.cat50FareRuleInfoResponseData = value;
    }

}
