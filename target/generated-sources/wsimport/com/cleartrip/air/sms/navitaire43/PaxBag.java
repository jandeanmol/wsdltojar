
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PaxBag complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaxBag">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="PassengerNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="ArrivalStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaggageStatus" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}BaggageStatus" minOccurs="0"/>
 *         &lt;element name="CompartmentID" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="OSTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OSTagDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxBag", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "passengerNumber",
    "arrivalStation",
    "departureStation",
    "baggageStatus",
    "compartmentID",
    "osTag",
    "osTagDate"
})
public class PaxBag
    extends StateMessage
{

    @XmlElement(name = "PassengerNumber")
    protected Short passengerNumber;
    @XmlElementRef(name = "ArrivalStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalStation;
    @XmlElementRef(name = "DepartureStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureStation;
    @XmlElement(name = "BaggageStatus")
    protected BaggageStatus baggageStatus;
    @XmlElement(name = "CompartmentID")
    protected Short compartmentID;
    @XmlElementRef(name = "OSTag", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> osTag;
    @XmlElement(name = "OSTagDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar osTagDate;

    /**
     * Gets the value of the passengerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPassengerNumber() {
        return passengerNumber;
    }

    /**
     * Sets the value of the passengerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPassengerNumber(Short value) {
        this.passengerNumber = value;
    }

    /**
     * Gets the value of the arrivalStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalStation() {
        return arrivalStation;
    }

    /**
     * Sets the value of the arrivalStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalStation(JAXBElement<String> value) {
        this.arrivalStation = value;
    }

    /**
     * Gets the value of the departureStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureStation() {
        return departureStation;
    }

    /**
     * Sets the value of the departureStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureStation(JAXBElement<String> value) {
        this.departureStation = value;
    }

    /**
     * Gets the value of the baggageStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BaggageStatus }
     *     
     */
    public BaggageStatus getBaggageStatus() {
        return baggageStatus;
    }

    /**
     * Sets the value of the baggageStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BaggageStatus }
     *     
     */
    public void setBaggageStatus(BaggageStatus value) {
        this.baggageStatus = value;
    }

    /**
     * Gets the value of the compartmentID property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCompartmentID() {
        return compartmentID;
    }

    /**
     * Sets the value of the compartmentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCompartmentID(Short value) {
        this.compartmentID = value;
    }

    /**
     * Gets the value of the osTag property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOSTag() {
        return osTag;
    }

    /**
     * Sets the value of the osTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOSTag(JAXBElement<String> value) {
        this.osTag = value;
    }

    /**
     * Gets the value of the osTagDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOSTagDate() {
        return osTagDate;
    }

    /**
     * Sets the value of the osTagDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOSTagDate(XMLGregorianCalendar value) {
        this.osTagDate = value;
    }

}
