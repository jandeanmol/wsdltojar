
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClassStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ClassStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Active"/>
 *     &lt;enumeration value="InActive"/>
 *     &lt;enumeration value="AVSOpen"/>
 *     &lt;enumeration value="AVSOnRequest"/>
 *     &lt;enumeration value="AVSClosed"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ClassStatus", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum ClassStatus {

    @XmlEnumValue("Active")
    ACTIVE("Active"),
    @XmlEnumValue("InActive")
    IN_ACTIVE("InActive"),
    @XmlEnumValue("AVSOpen")
    AVS_OPEN("AVSOpen"),
    @XmlEnumValue("AVSOnRequest")
    AVS_ON_REQUEST("AVSOnRequest"),
    @XmlEnumValue("AVSClosed")
    AVS_CLOSED("AVSClosed"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    ClassStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ClassStatus fromValue(String v) {
        for (ClassStatus c: ClassStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
