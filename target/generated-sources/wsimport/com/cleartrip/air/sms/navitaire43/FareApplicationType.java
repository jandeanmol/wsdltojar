
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FareApplicationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FareApplicationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Route"/>
 *     &lt;enumeration value="Sector"/>
 *     &lt;enumeration value="Governing"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FareApplicationType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum FareApplicationType {

    @XmlEnumValue("Route")
    ROUTE("Route"),
    @XmlEnumValue("Sector")
    SECTOR("Sector"),
    @XmlEnumValue("Governing")
    GOVERNING("Governing"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    FareApplicationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FareApplicationType fromValue(String v) {
        for (FareApplicationType c: FareApplicationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
