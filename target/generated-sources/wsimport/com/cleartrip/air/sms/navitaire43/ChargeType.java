
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ChargeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FarePrice"/>
 *     &lt;enumeration value="Discount"/>
 *     &lt;enumeration value="IncludedTravelFee"/>
 *     &lt;enumeration value="IncludedTax"/>
 *     &lt;enumeration value="TravelFee"/>
 *     &lt;enumeration value="Tax"/>
 *     &lt;enumeration value="ServiceCharge"/>
 *     &lt;enumeration value="PromotionDiscount"/>
 *     &lt;enumeration value="ConnectionAdjustmentAmount"/>
 *     &lt;enumeration value="AddOnServicePrice"/>
 *     &lt;enumeration value="IncludedAddOnServiceFee"/>
 *     &lt;enumeration value="AddOnServiceFee"/>
 *     &lt;enumeration value="Calculated"/>
 *     &lt;enumeration value="Note"/>
 *     &lt;enumeration value="AddOnServiceMarkup"/>
 *     &lt;enumeration value="FareSurcharge"/>
 *     &lt;enumeration value="Loyalty"/>
 *     &lt;enumeration value="FarePoints"/>
 *     &lt;enumeration value="DiscountPoints"/>
 *     &lt;enumeration value="AddOnServiceCancelFee"/>
 *     &lt;enumeration value="Points"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChargeType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum ChargeType {

    @XmlEnumValue("FarePrice")
    FARE_PRICE("FarePrice"),
    @XmlEnumValue("Discount")
    DISCOUNT("Discount"),
    @XmlEnumValue("IncludedTravelFee")
    INCLUDED_TRAVEL_FEE("IncludedTravelFee"),
    @XmlEnumValue("IncludedTax")
    INCLUDED_TAX("IncludedTax"),
    @XmlEnumValue("TravelFee")
    TRAVEL_FEE("TravelFee"),
    @XmlEnumValue("Tax")
    TAX("Tax"),
    @XmlEnumValue("ServiceCharge")
    SERVICE_CHARGE("ServiceCharge"),
    @XmlEnumValue("PromotionDiscount")
    PROMOTION_DISCOUNT("PromotionDiscount"),
    @XmlEnumValue("ConnectionAdjustmentAmount")
    CONNECTION_ADJUSTMENT_AMOUNT("ConnectionAdjustmentAmount"),
    @XmlEnumValue("AddOnServicePrice")
    ADD_ON_SERVICE_PRICE("AddOnServicePrice"),
    @XmlEnumValue("IncludedAddOnServiceFee")
    INCLUDED_ADD_ON_SERVICE_FEE("IncludedAddOnServiceFee"),
    @XmlEnumValue("AddOnServiceFee")
    ADD_ON_SERVICE_FEE("AddOnServiceFee"),
    @XmlEnumValue("Calculated")
    CALCULATED("Calculated"),
    @XmlEnumValue("Note")
    NOTE("Note"),
    @XmlEnumValue("AddOnServiceMarkup")
    ADD_ON_SERVICE_MARKUP("AddOnServiceMarkup"),
    @XmlEnumValue("FareSurcharge")
    FARE_SURCHARGE("FareSurcharge"),
    @XmlEnumValue("Loyalty")
    LOYALTY("Loyalty"),
    @XmlEnumValue("FarePoints")
    FARE_POINTS("FarePoints"),
    @XmlEnumValue("DiscountPoints")
    DISCOUNT_POINTS("DiscountPoints"),
    @XmlEnumValue("AddOnServiceCancelFee")
    ADD_ON_SERVICE_CANCEL_FEE("AddOnServiceCancelFee"),
    @XmlEnumValue("Points")
    POINTS("Points"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    ChargeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChargeType fromValue(String v) {
        for (ChargeType c: ChargeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
