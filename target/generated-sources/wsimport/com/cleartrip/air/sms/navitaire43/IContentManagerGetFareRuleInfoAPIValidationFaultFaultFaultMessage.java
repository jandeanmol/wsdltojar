
package com.cleartrip.air.sms.navitaire43;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b14002
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "APIValidationFault", targetNamespace = "http://schemas.navitaire.com/WebServices/FaultContracts")
public class IContentManagerGetFareRuleInfoAPIValidationFaultFaultFaultMessage
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private APIValidationFault faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public IContentManagerGetFareRuleInfoAPIValidationFaultFaultFaultMessage(String message, APIValidationFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public IContentManagerGetFareRuleInfoAPIValidationFaultFaultFaultMessage(String message, APIValidationFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: com.cleartrip.air.sms.navitaire43.APIValidationFault
     */
    public APIValidationFault getFaultInfo() {
        return faultInfo;
    }

}
