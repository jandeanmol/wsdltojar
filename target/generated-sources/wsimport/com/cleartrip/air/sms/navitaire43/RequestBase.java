
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActiveOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PageSize" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="LastID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="LastCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CultureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GetTotalCount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestBase", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", propOrder = {
    "activeOnly",
    "pageSize",
    "lastID",
    "lastCode",
    "cultureCode",
    "getTotalCount"
})
@XmlSeeAlso({
    ContentRequestData.class
})
public class RequestBase {

    @XmlElement(name = "ActiveOnly")
    protected Boolean activeOnly;
    @XmlElement(name = "PageSize")
    protected Short pageSize;
    @XmlElement(name = "LastID")
    protected Long lastID;
    @XmlElementRef(name = "LastCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastCode;
    @XmlElementRef(name = "CultureCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cultureCode;
    @XmlElement(name = "GetTotalCount")
    protected Boolean getTotalCount;

    /**
     * Gets the value of the activeOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isActiveOnly() {
        return activeOnly;
    }

    /**
     * Sets the value of the activeOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActiveOnly(Boolean value) {
        this.activeOnly = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPageSize(Short value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the lastID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLastID() {
        return lastID;
    }

    /**
     * Sets the value of the lastID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLastID(Long value) {
        this.lastID = value;
    }

    /**
     * Gets the value of the lastCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastCode() {
        return lastCode;
    }

    /**
     * Sets the value of the lastCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastCode(JAXBElement<String> value) {
        this.lastCode = value;
    }

    /**
     * Gets the value of the cultureCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCultureCode() {
        return cultureCode;
    }

    /**
     * Sets the value of the cultureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCultureCode(JAXBElement<String> value) {
        this.cultureCode = value;
    }

    /**
     * Gets the value of the getTotalCount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGetTotalCount() {
        return getTotalCount;
    }

    /**
     * Sets the value of the getTotalCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetTotalCount(Boolean value) {
        this.getTotalCount = value;
    }

}
