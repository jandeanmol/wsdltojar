
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Segment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Segment">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="ActionStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CabinOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangeReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SegmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STD" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="International" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="FlightDesignator" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common}FlightDesignator" minOccurs="0"/>
 *         &lt;element name="XrefFlightDesignator" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common}FlightDesignator" minOccurs="0"/>
 *         &lt;element name="Fares" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfFare" minOccurs="0"/>
 *         &lt;element name="Legs" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfLeg" minOccurs="0"/>
 *         &lt;element name="PaxBags" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxBag" minOccurs="0"/>
 *         &lt;element name="PaxSeats" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxSeat" minOccurs="0"/>
 *         &lt;element name="PaxSSRs" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxSSR" minOccurs="0"/>
 *         &lt;element name="PaxSegments" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxSegment" minOccurs="0"/>
 *         &lt;element name="PaxTickets" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxTicket" minOccurs="0"/>
 *         &lt;element name="PaxSeatPreferences" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxSeatPreference" minOccurs="0"/>
 *         &lt;element name="SalesDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="SegmentSellKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxScores" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxScore" minOccurs="0"/>
 *         &lt;element name="ChannelType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}ChannelType" minOccurs="0"/>
 *         &lt;element name="AvailabilitySourceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InventorySourceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Segment", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "actionStatusCode",
    "arrivalStation",
    "cabinOfService",
    "changeReasonCode",
    "departureStation",
    "priorityCode",
    "segmentType",
    "sta",
    "std",
    "international",
    "flightDesignator",
    "xrefFlightDesignator",
    "fares",
    "legs",
    "paxBags",
    "paxSeats",
    "paxSSRs",
    "paxSegments",
    "paxTickets",
    "paxSeatPreferences",
    "salesDate",
    "segmentSellKey",
    "paxScores",
    "channelType",
    "availabilitySourceCode",
    "inventorySourceCode"
})
public class Segment
    extends StateMessage
{

    @XmlElementRef(name = "ActionStatusCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actionStatusCode;
    @XmlElementRef(name = "ArrivalStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalStation;
    @XmlElementRef(name = "CabinOfService", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cabinOfService;
    @XmlElementRef(name = "ChangeReasonCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeReasonCode;
    @XmlElementRef(name = "DepartureStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureStation;
    @XmlElementRef(name = "PriorityCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> priorityCode;
    @XmlElementRef(name = "SegmentType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> segmentType;
    @XmlElement(name = "STA")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sta;
    @XmlElement(name = "STD")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar std;
    @XmlElement(name = "International")
    protected Boolean international;
    @XmlElementRef(name = "FlightDesignator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<FlightDesignator> flightDesignator;
    @XmlElementRef(name = "XrefFlightDesignator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<FlightDesignator> xrefFlightDesignator;
    @XmlElementRef(name = "Fares", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFare> fares;
    @XmlElementRef(name = "Legs", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLeg> legs;
    @XmlElementRef(name = "PaxBags", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxBag> paxBags;
    @XmlElementRef(name = "PaxSeats", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxSeat> paxSeats;
    @XmlElementRef(name = "PaxSSRs", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxSSR> paxSSRs;
    @XmlElementRef(name = "PaxSegments", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxSegment> paxSegments;
    @XmlElementRef(name = "PaxTickets", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxTicket> paxTickets;
    @XmlElementRef(name = "PaxSeatPreferences", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxSeatPreference> paxSeatPreferences;
    @XmlElement(name = "SalesDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar salesDate;
    @XmlElementRef(name = "SegmentSellKey", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> segmentSellKey;
    @XmlElementRef(name = "PaxScores", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxScore> paxScores;
    @XmlElement(name = "ChannelType")
    protected ChannelType channelType;
    @XmlElementRef(name = "AvailabilitySourceCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> availabilitySourceCode;
    @XmlElementRef(name = "InventorySourceCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> inventorySourceCode;

    /**
     * Gets the value of the actionStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActionStatusCode() {
        return actionStatusCode;
    }

    /**
     * Sets the value of the actionStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActionStatusCode(JAXBElement<String> value) {
        this.actionStatusCode = value;
    }

    /**
     * Gets the value of the arrivalStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalStation() {
        return arrivalStation;
    }

    /**
     * Sets the value of the arrivalStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalStation(JAXBElement<String> value) {
        this.arrivalStation = value;
    }

    /**
     * Gets the value of the cabinOfService property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCabinOfService() {
        return cabinOfService;
    }

    /**
     * Sets the value of the cabinOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCabinOfService(JAXBElement<String> value) {
        this.cabinOfService = value;
    }

    /**
     * Gets the value of the changeReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeReasonCode() {
        return changeReasonCode;
    }

    /**
     * Sets the value of the changeReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeReasonCode(JAXBElement<String> value) {
        this.changeReasonCode = value;
    }

    /**
     * Gets the value of the departureStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureStation() {
        return departureStation;
    }

    /**
     * Sets the value of the departureStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureStation(JAXBElement<String> value) {
        this.departureStation = value;
    }

    /**
     * Gets the value of the priorityCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPriorityCode() {
        return priorityCode;
    }

    /**
     * Sets the value of the priorityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPriorityCode(JAXBElement<String> value) {
        this.priorityCode = value;
    }

    /**
     * Gets the value of the segmentType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSegmentType() {
        return segmentType;
    }

    /**
     * Sets the value of the segmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSegmentType(JAXBElement<String> value) {
        this.segmentType = value;
    }

    /**
     * Gets the value of the sta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTA() {
        return sta;
    }

    /**
     * Sets the value of the sta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTA(XMLGregorianCalendar value) {
        this.sta = value;
    }

    /**
     * Gets the value of the std property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTD() {
        return std;
    }

    /**
     * Sets the value of the std property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTD(XMLGregorianCalendar value) {
        this.std = value;
    }

    /**
     * Gets the value of the international property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInternational() {
        return international;
    }

    /**
     * Sets the value of the international property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInternational(Boolean value) {
        this.international = value;
    }

    /**
     * Gets the value of the flightDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FlightDesignator }{@code >}
     *     
     */
    public JAXBElement<FlightDesignator> getFlightDesignator() {
        return flightDesignator;
    }

    /**
     * Sets the value of the flightDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FlightDesignator }{@code >}
     *     
     */
    public void setFlightDesignator(JAXBElement<FlightDesignator> value) {
        this.flightDesignator = value;
    }

    /**
     * Gets the value of the xrefFlightDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FlightDesignator }{@code >}
     *     
     */
    public JAXBElement<FlightDesignator> getXrefFlightDesignator() {
        return xrefFlightDesignator;
    }

    /**
     * Sets the value of the xrefFlightDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FlightDesignator }{@code >}
     *     
     */
    public void setXrefFlightDesignator(JAXBElement<FlightDesignator> value) {
        this.xrefFlightDesignator = value;
    }

    /**
     * Gets the value of the fares property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFare }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFare> getFares() {
        return fares;
    }

    /**
     * Sets the value of the fares property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFare }{@code >}
     *     
     */
    public void setFares(JAXBElement<ArrayOfFare> value) {
        this.fares = value;
    }

    /**
     * Gets the value of the legs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLeg }{@code >}
     *     
     */
    public JAXBElement<ArrayOfLeg> getLegs() {
        return legs;
    }

    /**
     * Sets the value of the legs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLeg }{@code >}
     *     
     */
    public void setLegs(JAXBElement<ArrayOfLeg> value) {
        this.legs = value;
    }

    /**
     * Gets the value of the paxBags property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxBag }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxBag> getPaxBags() {
        return paxBags;
    }

    /**
     * Sets the value of the paxBags property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxBag }{@code >}
     *     
     */
    public void setPaxBags(JAXBElement<ArrayOfPaxBag> value) {
        this.paxBags = value;
    }

    /**
     * Gets the value of the paxSeats property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSeat }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxSeat> getPaxSeats() {
        return paxSeats;
    }

    /**
     * Sets the value of the paxSeats property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSeat }{@code >}
     *     
     */
    public void setPaxSeats(JAXBElement<ArrayOfPaxSeat> value) {
        this.paxSeats = value;
    }

    /**
     * Gets the value of the paxSSRs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSSR }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxSSR> getPaxSSRs() {
        return paxSSRs;
    }

    /**
     * Sets the value of the paxSSRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSSR }{@code >}
     *     
     */
    public void setPaxSSRs(JAXBElement<ArrayOfPaxSSR> value) {
        this.paxSSRs = value;
    }

    /**
     * Gets the value of the paxSegments property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSegment }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxSegment> getPaxSegments() {
        return paxSegments;
    }

    /**
     * Sets the value of the paxSegments property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSegment }{@code >}
     *     
     */
    public void setPaxSegments(JAXBElement<ArrayOfPaxSegment> value) {
        this.paxSegments = value;
    }

    /**
     * Gets the value of the paxTickets property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxTicket }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxTicket> getPaxTickets() {
        return paxTickets;
    }

    /**
     * Sets the value of the paxTickets property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxTicket }{@code >}
     *     
     */
    public void setPaxTickets(JAXBElement<ArrayOfPaxTicket> value) {
        this.paxTickets = value;
    }

    /**
     * Gets the value of the paxSeatPreferences property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSeatPreference }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxSeatPreference> getPaxSeatPreferences() {
        return paxSeatPreferences;
    }

    /**
     * Sets the value of the paxSeatPreferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxSeatPreference }{@code >}
     *     
     */
    public void setPaxSeatPreferences(JAXBElement<ArrayOfPaxSeatPreference> value) {
        this.paxSeatPreferences = value;
    }

    /**
     * Gets the value of the salesDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSalesDate() {
        return salesDate;
    }

    /**
     * Sets the value of the salesDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSalesDate(XMLGregorianCalendar value) {
        this.salesDate = value;
    }

    /**
     * Gets the value of the segmentSellKey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSegmentSellKey() {
        return segmentSellKey;
    }

    /**
     * Sets the value of the segmentSellKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSegmentSellKey(JAXBElement<String> value) {
        this.segmentSellKey = value;
    }

    /**
     * Gets the value of the paxScores property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxScore }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxScore> getPaxScores() {
        return paxScores;
    }

    /**
     * Sets the value of the paxScores property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxScore }{@code >}
     *     
     */
    public void setPaxScores(JAXBElement<ArrayOfPaxScore> value) {
        this.paxScores = value;
    }

    /**
     * Gets the value of the channelType property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelType }
     *     
     */
    public ChannelType getChannelType() {
        return channelType;
    }

    /**
     * Sets the value of the channelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelType }
     *     
     */
    public void setChannelType(ChannelType value) {
        this.channelType = value;
    }

    /**
     * Gets the value of the availabilitySourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAvailabilitySourceCode() {
        return availabilitySourceCode;
    }

    /**
     * Sets the value of the availabilitySourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAvailabilitySourceCode(JAXBElement<String> value) {
        this.availabilitySourceCode = value;
    }

    /**
     * Gets the value of the inventorySourceCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInventorySourceCode() {
        return inventorySourceCode;
    }

    /**
     * Sets the value of the inventorySourceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInventorySourceCode(JAXBElement<String> value) {
        this.inventorySourceCode = value;
    }

}
