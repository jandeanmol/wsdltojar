
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContentResponseData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContentResponseData">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="Contents" type="{http://schemas.navitaire.com/WebServices/DataContracts/Content}ArrayOfContent" minOccurs="0"/>
 *         &lt;element name="OtherServiceInformations" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common}ArrayOfOtherServiceInformation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContentResponseData", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", propOrder = {
    "contents",
    "otherServiceInformations"
})
public class ContentResponseData
    extends ResponseBase
{

    @XmlElementRef(name = "Contents", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfContent> contents;
    @XmlElementRef(name = "OtherServiceInformations", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfOtherServiceInformation> otherServiceInformations;

    /**
     * Gets the value of the contents property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfContent }{@code >}
     *     
     */
    public JAXBElement<ArrayOfContent> getContents() {
        return contents;
    }

    /**
     * Sets the value of the contents property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfContent }{@code >}
     *     
     */
    public void setContents(JAXBElement<ArrayOfContent> value) {
        this.contents = value;
    }

    /**
     * Gets the value of the otherServiceInformations property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfOtherServiceInformation }{@code >}
     *     
     */
    public JAXBElement<ArrayOfOtherServiceInformation> getOtherServiceInformations() {
        return otherServiceInformations;
    }

    /**
     * Sets the value of the otherServiceInformations property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfOtherServiceInformation }{@code >}
     *     
     */
    public void setOtherServiceInformations(JAXBElement<ArrayOfOtherServiceInformation> value) {
        this.otherServiceInformations = value;
    }

}
