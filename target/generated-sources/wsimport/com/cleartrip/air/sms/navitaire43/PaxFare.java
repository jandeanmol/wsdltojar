
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaxFare complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaxFare">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="PaxType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxDiscountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareDiscountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceCharges" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfBookingServiceCharge" minOccurs="0"/>
 *         &lt;element name="TicketFareBasisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProofOfStatusRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxFare", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "paxType",
    "paxDiscountCode",
    "fareDiscountCode",
    "serviceCharges",
    "ticketFareBasisCode",
    "proofOfStatusRequired"
})
public class PaxFare
    extends StateMessage
{

    @XmlElementRef(name = "PaxType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paxType;
    @XmlElementRef(name = "PaxDiscountCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> paxDiscountCode;
    @XmlElementRef(name = "FareDiscountCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fareDiscountCode;
    @XmlElementRef(name = "ServiceCharges", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfBookingServiceCharge> serviceCharges;
    @XmlElementRef(name = "TicketFareBasisCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ticketFareBasisCode;
    @XmlElement(name = "ProofOfStatusRequired")
    protected Boolean proofOfStatusRequired;

    /**
     * Gets the value of the paxType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaxType() {
        return paxType;
    }

    /**
     * Sets the value of the paxType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaxType(JAXBElement<String> value) {
        this.paxType = value;
    }

    /**
     * Gets the value of the paxDiscountCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaxDiscountCode() {
        return paxDiscountCode;
    }

    /**
     * Sets the value of the paxDiscountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaxDiscountCode(JAXBElement<String> value) {
        this.paxDiscountCode = value;
    }

    /**
     * Gets the value of the fareDiscountCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFareDiscountCode() {
        return fareDiscountCode;
    }

    /**
     * Sets the value of the fareDiscountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFareDiscountCode(JAXBElement<String> value) {
        this.fareDiscountCode = value;
    }

    /**
     * Gets the value of the serviceCharges property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfBookingServiceCharge }{@code >}
     *     
     */
    public JAXBElement<ArrayOfBookingServiceCharge> getServiceCharges() {
        return serviceCharges;
    }

    /**
     * Sets the value of the serviceCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfBookingServiceCharge }{@code >}
     *     
     */
    public void setServiceCharges(JAXBElement<ArrayOfBookingServiceCharge> value) {
        this.serviceCharges = value;
    }

    /**
     * Gets the value of the ticketFareBasisCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTicketFareBasisCode() {
        return ticketFareBasisCode;
    }

    /**
     * Sets the value of the ticketFareBasisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTicketFareBasisCode(JAXBElement<String> value) {
        this.ticketFareBasisCode = value;
    }

    /**
     * Gets the value of the proofOfStatusRequired property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProofOfStatusRequired() {
        return proofOfStatusRequired;
    }

    /**
     * Sets the value of the proofOfStatusRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProofOfStatusRequired(Boolean value) {
        this.proofOfStatusRequired = value;
    }

}
