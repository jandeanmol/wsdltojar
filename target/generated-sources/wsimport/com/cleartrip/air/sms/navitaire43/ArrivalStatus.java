
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrivalStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ArrivalStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="Cancelled"/>
 *     &lt;enumeration value="Arrived"/>
 *     &lt;enumeration value="SeeAgent"/>
 *     &lt;enumeration value="Delayed"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ArrivalStatus", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum ArrivalStatus {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Cancelled")
    CANCELLED("Cancelled"),
    @XmlEnumValue("Arrived")
    ARRIVED("Arrived"),
    @XmlEnumValue("SeeAgent")
    SEE_AGENT("SeeAgent"),
    @XmlEnumValue("Delayed")
    DELAYED("Delayed"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    ArrivalStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ArrivalStatus fromValue(String v) {
        for (ArrivalStatus c: ArrivalStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
