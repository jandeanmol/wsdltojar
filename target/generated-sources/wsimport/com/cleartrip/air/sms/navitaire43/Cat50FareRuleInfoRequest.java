
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Cat50FareRuleInfoRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cat50FareRuleInfoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Journey" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}Journey" minOccurs="0"/>
 *         &lt;element name="PaxPriceTypeList" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxPriceType" minOccurs="0"/>
 *         &lt;element name="SegmentIndex" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CultureCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cat50FareRuleInfoRequest", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", propOrder = {
    "journey",
    "paxPriceTypeList",
    "segmentIndex",
    "cultureCode"
})
public class Cat50FareRuleInfoRequest {

    @XmlElementRef(name = "Journey", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<Journey> journey;
    @XmlElementRef(name = "PaxPriceTypeList", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxPriceType> paxPriceTypeList;
    @XmlElement(name = "SegmentIndex")
    protected Integer segmentIndex;
    @XmlElementRef(name = "CultureCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cultureCode;

    /**
     * Gets the value of the journey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Journey }{@code >}
     *     
     */
    public JAXBElement<Journey> getJourney() {
        return journey;
    }

    /**
     * Sets the value of the journey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Journey }{@code >}
     *     
     */
    public void setJourney(JAXBElement<Journey> value) {
        this.journey = value;
    }

    /**
     * Gets the value of the paxPriceTypeList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxPriceType }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxPriceType> getPaxPriceTypeList() {
        return paxPriceTypeList;
    }

    /**
     * Sets the value of the paxPriceTypeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxPriceType }{@code >}
     *     
     */
    public void setPaxPriceTypeList(JAXBElement<ArrayOfPaxPriceType> value) {
        this.paxPriceTypeList = value;
    }

    /**
     * Gets the value of the segmentIndex property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSegmentIndex() {
        return segmentIndex;
    }

    /**
     * Sets the value of the segmentIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSegmentIndex(Integer value) {
        this.segmentIndex = value;
    }

    /**
     * Gets the value of the cultureCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCultureCode() {
        return cultureCode;
    }

    /**
     * Sets the value of the cultureCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCultureCode(JAXBElement<String> value) {
        this.cultureCode = value;
    }

}
