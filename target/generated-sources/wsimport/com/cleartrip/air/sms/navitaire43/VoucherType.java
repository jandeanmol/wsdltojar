
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoucherType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VoucherType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Credit"/>
 *     &lt;enumeration value="SingleUseCredit"/>
 *     &lt;enumeration value="Service"/>
 *     &lt;enumeration value="SingleUse"/>
 *     &lt;enumeration value="MultiUse"/>
 *     &lt;enumeration value="SingleUseNegativeAdjustment"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VoucherType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum VoucherType {

    @XmlEnumValue("Credit")
    CREDIT("Credit"),
    @XmlEnumValue("SingleUseCredit")
    SINGLE_USE_CREDIT("SingleUseCredit"),
    @XmlEnumValue("Service")
    SERVICE("Service"),
    @XmlEnumValue("SingleUse")
    SINGLE_USE("SingleUse"),
    @XmlEnumValue("MultiUse")
    MULTI_USE("MultiUse"),
    @XmlEnumValue("SingleUseNegativeAdjustment")
    SINGLE_USE_NEGATIVE_ADJUSTMENT("SingleUseNegativeAdjustment"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    VoucherType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoucherType fromValue(String v) {
        for (VoucherType c: VoucherType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
