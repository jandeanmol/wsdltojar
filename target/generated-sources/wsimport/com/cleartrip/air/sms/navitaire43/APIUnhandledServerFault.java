
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for APIUnhandledServerFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="APIUnhandledServerFault">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/FaultContracts}APIWarningFault">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "APIUnhandledServerFault", namespace = "http://schemas.navitaire.com/WebServices/FaultContracts")
public class APIUnhandledServerFault
    extends APIWarningFault
{


}
