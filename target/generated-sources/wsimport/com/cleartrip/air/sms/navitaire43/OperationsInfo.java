
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OperationsInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OperationsInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="ActualArrivalGate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActualDepartureGate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActualOffBlockTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ActualOnBlockTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ActualTouchDownTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="AirborneTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ArrivalGate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalStatus" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}ArrivalStatus" minOccurs="0"/>
 *         &lt;element name="BaggageClaim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureGate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureStatus" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}DepartureStatus" minOccurs="0"/>
 *         &lt;element name="ETA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ETD" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STD" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="TailNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationsInfo", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "actualArrivalGate",
    "actualDepartureGate",
    "actualOffBlockTime",
    "actualOnBlockTime",
    "actualTouchDownTime",
    "airborneTime",
    "arrivalGate",
    "arrivalNote",
    "arrivalStatus",
    "baggageClaim",
    "departureGate",
    "departureNote",
    "departureStatus",
    "eta",
    "etd",
    "sta",
    "std",
    "tailNumber"
})
public class OperationsInfo
    extends StateMessage
{

    @XmlElementRef(name = "ActualArrivalGate", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actualArrivalGate;
    @XmlElementRef(name = "ActualDepartureGate", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actualDepartureGate;
    @XmlElement(name = "ActualOffBlockTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualOffBlockTime;
    @XmlElement(name = "ActualOnBlockTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualOnBlockTime;
    @XmlElement(name = "ActualTouchDownTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actualTouchDownTime;
    @XmlElement(name = "AirborneTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar airborneTime;
    @XmlElementRef(name = "ArrivalGate", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalGate;
    @XmlElementRef(name = "ArrivalNote", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalNote;
    @XmlElement(name = "ArrivalStatus")
    protected ArrivalStatus arrivalStatus;
    @XmlElementRef(name = "BaggageClaim", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baggageClaim;
    @XmlElementRef(name = "DepartureGate", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureGate;
    @XmlElementRef(name = "DepartureNote", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureNote;
    @XmlElement(name = "DepartureStatus")
    protected DepartureStatus departureStatus;
    @XmlElement(name = "ETA")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eta;
    @XmlElement(name = "ETD")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar etd;
    @XmlElement(name = "STA")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sta;
    @XmlElement(name = "STD")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar std;
    @XmlElementRef(name = "TailNumber", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tailNumber;

    /**
     * Gets the value of the actualArrivalGate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActualArrivalGate() {
        return actualArrivalGate;
    }

    /**
     * Sets the value of the actualArrivalGate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActualArrivalGate(JAXBElement<String> value) {
        this.actualArrivalGate = value;
    }

    /**
     * Gets the value of the actualDepartureGate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActualDepartureGate() {
        return actualDepartureGate;
    }

    /**
     * Sets the value of the actualDepartureGate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActualDepartureGate(JAXBElement<String> value) {
        this.actualDepartureGate = value;
    }

    /**
     * Gets the value of the actualOffBlockTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualOffBlockTime() {
        return actualOffBlockTime;
    }

    /**
     * Sets the value of the actualOffBlockTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualOffBlockTime(XMLGregorianCalendar value) {
        this.actualOffBlockTime = value;
    }

    /**
     * Gets the value of the actualOnBlockTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualOnBlockTime() {
        return actualOnBlockTime;
    }

    /**
     * Sets the value of the actualOnBlockTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualOnBlockTime(XMLGregorianCalendar value) {
        this.actualOnBlockTime = value;
    }

    /**
     * Gets the value of the actualTouchDownTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActualTouchDownTime() {
        return actualTouchDownTime;
    }

    /**
     * Sets the value of the actualTouchDownTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActualTouchDownTime(XMLGregorianCalendar value) {
        this.actualTouchDownTime = value;
    }

    /**
     * Gets the value of the airborneTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAirborneTime() {
        return airborneTime;
    }

    /**
     * Sets the value of the airborneTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAirborneTime(XMLGregorianCalendar value) {
        this.airborneTime = value;
    }

    /**
     * Gets the value of the arrivalGate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalGate() {
        return arrivalGate;
    }

    /**
     * Sets the value of the arrivalGate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalGate(JAXBElement<String> value) {
        this.arrivalGate = value;
    }

    /**
     * Gets the value of the arrivalNote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalNote() {
        return arrivalNote;
    }

    /**
     * Sets the value of the arrivalNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalNote(JAXBElement<String> value) {
        this.arrivalNote = value;
    }

    /**
     * Gets the value of the arrivalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ArrivalStatus }
     *     
     */
    public ArrivalStatus getArrivalStatus() {
        return arrivalStatus;
    }

    /**
     * Sets the value of the arrivalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrivalStatus }
     *     
     */
    public void setArrivalStatus(ArrivalStatus value) {
        this.arrivalStatus = value;
    }

    /**
     * Gets the value of the baggageClaim property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaggageClaim() {
        return baggageClaim;
    }

    /**
     * Sets the value of the baggageClaim property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaggageClaim(JAXBElement<String> value) {
        this.baggageClaim = value;
    }

    /**
     * Gets the value of the departureGate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureGate() {
        return departureGate;
    }

    /**
     * Sets the value of the departureGate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureGate(JAXBElement<String> value) {
        this.departureGate = value;
    }

    /**
     * Gets the value of the departureNote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureNote() {
        return departureNote;
    }

    /**
     * Sets the value of the departureNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureNote(JAXBElement<String> value) {
        this.departureNote = value;
    }

    /**
     * Gets the value of the departureStatus property.
     * 
     * @return
     *     possible object is
     *     {@link DepartureStatus }
     *     
     */
    public DepartureStatus getDepartureStatus() {
        return departureStatus;
    }

    /**
     * Sets the value of the departureStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartureStatus }
     *     
     */
    public void setDepartureStatus(DepartureStatus value) {
        this.departureStatus = value;
    }

    /**
     * Gets the value of the eta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getETA() {
        return eta;
    }

    /**
     * Sets the value of the eta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setETA(XMLGregorianCalendar value) {
        this.eta = value;
    }

    /**
     * Gets the value of the etd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getETD() {
        return etd;
    }

    /**
     * Sets the value of the etd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setETD(XMLGregorianCalendar value) {
        this.etd = value;
    }

    /**
     * Gets the value of the sta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTA() {
        return sta;
    }

    /**
     * Sets the value of the sta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTA(XMLGregorianCalendar value) {
        this.sta = value;
    }

    /**
     * Gets the value of the std property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTD() {
        return std;
    }

    /**
     * Sets the value of the std property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTD(XMLGregorianCalendar value) {
        this.std = value;
    }

    /**
     * Gets the value of the tailNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTailNumber() {
        return tailNumber;
    }

    /**
     * Sets the value of the tailNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTailNumber(JAXBElement<String> value) {
        this.tailNumber = value;
    }

}
