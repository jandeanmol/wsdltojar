
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidationResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidationResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Annotations" type="{http://schemas.datacontract.org/2004/07/Navitaire.Ncl.Collections}SerializableDictionaryOfstringstring" minOccurs="0"/>
 *         &lt;element name="FailedValidationDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Successful" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidationResult", namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", propOrder = {
    "annotations",
    "failedValidationDescription",
    "key",
    "successful"
})
public class ValidationResult {

    @XmlElementRef(name = "Annotations", namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", type = JAXBElement.class, required = false)
    protected JAXBElement<SerializableDictionaryOfstringstring> annotations;
    @XmlElementRef(name = "FailedValidationDescription", namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", type = JAXBElement.class, required = false)
    protected JAXBElement<String> failedValidationDescription;
    @XmlElementRef(name = "Key", namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", type = JAXBElement.class, required = false)
    protected JAXBElement<String> key;
    @XmlElement(name = "Successful")
    protected Boolean successful;

    /**
     * Gets the value of the annotations property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SerializableDictionaryOfstringstring }{@code >}
     *     
     */
    public JAXBElement<SerializableDictionaryOfstringstring> getAnnotations() {
        return annotations;
    }

    /**
     * Sets the value of the annotations property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SerializableDictionaryOfstringstring }{@code >}
     *     
     */
    public void setAnnotations(JAXBElement<SerializableDictionaryOfstringstring> value) {
        this.annotations = value;
    }

    /**
     * Gets the value of the failedValidationDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFailedValidationDescription() {
        return failedValidationDescription;
    }

    /**
     * Sets the value of the failedValidationDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFailedValidationDescription(JAXBElement<String> value) {
        this.failedValidationDescription = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKey(JAXBElement<String> value) {
        this.key = value;
    }

    /**
     * Gets the value of the successful property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSuccessful() {
        return successful;
    }

    /**
     * Sets the value of the successful property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSuccessful(Boolean value) {
        this.successful = value;
    }

}
