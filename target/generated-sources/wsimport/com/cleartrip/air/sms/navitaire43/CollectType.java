
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CollectType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CollectType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SellerChargeable"/>
 *     &lt;enumeration value="ExternalChargeable"/>
 *     &lt;enumeration value="SellerNonChargeable"/>
 *     &lt;enumeration value="ExternalNonChargeable"/>
 *     &lt;enumeration value="ExternalChargeableImmediate"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CollectType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum CollectType {

    @XmlEnumValue("SellerChargeable")
    SELLER_CHARGEABLE("SellerChargeable"),
    @XmlEnumValue("ExternalChargeable")
    EXTERNAL_CHARGEABLE("ExternalChargeable"),
    @XmlEnumValue("SellerNonChargeable")
    SELLER_NON_CHARGEABLE("SellerNonChargeable"),
    @XmlEnumValue("ExternalNonChargeable")
    EXTERNAL_NON_CHARGEABLE("ExternalNonChargeable"),
    @XmlEnumValue("ExternalChargeableImmediate")
    EXTERNAL_CHARGEABLE_IMMEDIATE("ExternalChargeableImmediate"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    CollectType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CollectType fromValue(String v) {
        for (CollectType c: CollectType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
