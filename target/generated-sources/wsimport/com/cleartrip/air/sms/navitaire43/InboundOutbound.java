
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InboundOutbound.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InboundOutbound">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Inbound"/>
 *     &lt;enumeration value="Outbound"/>
 *     &lt;enumeration value="Both"/>
 *     &lt;enumeration value="RoundFrom"/>
 *     &lt;enumeration value="RoundTo"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InboundOutbound", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum InboundOutbound {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Inbound")
    INBOUND("Inbound"),
    @XmlEnumValue("Outbound")
    OUTBOUND("Outbound"),
    @XmlEnumValue("Both")
    BOTH("Both"),
    @XmlEnumValue("RoundFrom")
    ROUND_FROM("RoundFrom"),
    @XmlEnumValue("RoundTo")
    ROUND_TO("RoundTo"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    InboundOutbound(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InboundOutbound fromValue(String v) {
        for (InboundOutbound c: InboundOutbound.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
