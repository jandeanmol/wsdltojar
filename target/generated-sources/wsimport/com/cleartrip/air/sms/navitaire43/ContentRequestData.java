
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContentRequestData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContentRequestData">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}RequestBase">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common}SearchString" minOccurs="0"/>
 *         &lt;element name="ContentType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}ContentType" minOccurs="0"/>
 *         &lt;element name="ContainerID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ContentID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="GetDetails" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContentRequestData", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", propOrder = {
    "name",
    "contentType",
    "containerID",
    "contentID",
    "getDetails"
})
public class ContentRequestData
    extends RequestBase
{

    @XmlElementRef(name = "Name", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", type = JAXBElement.class, required = false)
    protected JAXBElement<SearchString> name;
    @XmlElement(name = "ContentType")
    protected ContentType contentType;
    @XmlElement(name = "ContainerID")
    protected Long containerID;
    @XmlElement(name = "ContentID")
    protected Long contentID;
    @XmlElement(name = "GetDetails")
    protected Boolean getDetails;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SearchString }{@code >}
     *     
     */
    public JAXBElement<SearchString> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SearchString }{@code >}
     *     
     */
    public void setName(JAXBElement<SearchString> value) {
        this.name = value;
    }

    /**
     * Gets the value of the contentType property.
     * 
     * @return
     *     possible object is
     *     {@link ContentType }
     *     
     */
    public ContentType getContentType() {
        return contentType;
    }

    /**
     * Sets the value of the contentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContentType }
     *     
     */
    public void setContentType(ContentType value) {
        this.contentType = value;
    }

    /**
     * Gets the value of the containerID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContainerID() {
        return containerID;
    }

    /**
     * Sets the value of the containerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContainerID(Long value) {
        this.containerID = value;
    }

    /**
     * Gets the value of the contentID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContentID() {
        return contentID;
    }

    /**
     * Sets the value of the contentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContentID(Long value) {
        this.contentID = value;
    }

    /**
     * Gets the value of the getDetails property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGetDetails() {
        return getDetails;
    }

    /**
     * Sets the value of the getDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetDetails(Boolean value) {
        this.getDetails = value;
    }

}
