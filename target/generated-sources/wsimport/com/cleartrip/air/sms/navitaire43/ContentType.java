
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContentType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GeneralReference"/>
 *     &lt;enumeration value="News"/>
 *     &lt;enumeration value="ConsoleHelp"/>
 *     &lt;enumeration value="FareRuleReference"/>
 *     &lt;enumeration value="ReviewWithCustomer"/>
 *     &lt;enumeration value="Notices"/>
 *     &lt;enumeration value="Literature"/>
 *     &lt;enumeration value="Links"/>
 *     &lt;enumeration value="Image"/>
 *     &lt;enumeration value="PromotionReference"/>
 *     &lt;enumeration value="SubscriptionReference"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ContentType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum ContentType {

    @XmlEnumValue("GeneralReference")
    GENERAL_REFERENCE("GeneralReference"),
    @XmlEnumValue("News")
    NEWS("News"),
    @XmlEnumValue("ConsoleHelp")
    CONSOLE_HELP("ConsoleHelp"),
    @XmlEnumValue("FareRuleReference")
    FARE_RULE_REFERENCE("FareRuleReference"),
    @XmlEnumValue("ReviewWithCustomer")
    REVIEW_WITH_CUSTOMER("ReviewWithCustomer"),
    @XmlEnumValue("Notices")
    NOTICES("Notices"),
    @XmlEnumValue("Literature")
    LITERATURE("Literature"),
    @XmlEnumValue("Links")
    LINKS("Links"),
    @XmlEnumValue("Image")
    IMAGE("Image"),
    @XmlEnumValue("PromotionReference")
    PROMOTION_REFERENCE("PromotionReference"),
    @XmlEnumValue("SubscriptionReference")
    SUBSCRIPTION_REFERENCE("SubscriptionReference"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    ContentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContentType fromValue(String v) {
        for (ContentType c: ContentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
