
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SeatPreference.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SeatPreference">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Window"/>
 *     &lt;enumeration value="Aisle"/>
 *     &lt;enumeration value="NoPreference"/>
 *     &lt;enumeration value="Front"/>
 *     &lt;enumeration value="Rear"/>
 *     &lt;enumeration value="WindowFront"/>
 *     &lt;enumeration value="WindowRear"/>
 *     &lt;enumeration value="AisleFront"/>
 *     &lt;enumeration value="AisleRear"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SeatPreference", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum SeatPreference {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Window")
    WINDOW("Window"),
    @XmlEnumValue("Aisle")
    AISLE("Aisle"),
    @XmlEnumValue("NoPreference")
    NO_PREFERENCE("NoPreference"),
    @XmlEnumValue("Front")
    FRONT("Front"),
    @XmlEnumValue("Rear")
    REAR("Rear"),
    @XmlEnumValue("WindowFront")
    WINDOW_FRONT("WindowFront"),
    @XmlEnumValue("WindowRear")
    WINDOW_REAR("WindowRear"),
    @XmlEnumValue("AisleFront")
    AISLE_FRONT("AisleFront"),
    @XmlEnumValue("AisleRear")
    AISLE_REAR("AisleRear"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    SeatPreference(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SeatPreference fromValue(String v) {
        for (SeatPreference c: SeatPreference.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
