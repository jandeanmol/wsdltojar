
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferSessionResponseData" type="{http://schemas.navitaire.com/WebServices/DataContracts/Session}TransferSessionResponseData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferSessionResponseData"
})
@XmlRootElement(name = "TransferSessionResponse", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService")
public class TransferSessionResponse {

    @XmlElementRef(name = "TransferSessionResponseData", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", type = JAXBElement.class, required = false)
    protected JAXBElement<TransferSessionResponseData> transferSessionResponseData;

    /**
     * Gets the value of the transferSessionResponseData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TransferSessionResponseData }{@code >}
     *     
     */
    public JAXBElement<TransferSessionResponseData> getTransferSessionResponseData() {
        return transferSessionResponseData;
    }

    /**
     * Sets the value of the transferSessionResponseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TransferSessionResponseData }{@code >}
     *     
     */
    public void setTransferSessionResponseData(JAXBElement<TransferSessionResponseData> value) {
        this.transferSessionResponseData = value;
    }

}
