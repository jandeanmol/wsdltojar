
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="logonRequestData" type="{http://schemas.navitaire.com/WebServices/DataContracts/Session}LogonRequestData" minOccurs="0"/>
 *         &lt;element name="newPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "logonRequestData",
    "newPassword"
})
@XmlRootElement(name = "ChangePasswordRequest", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService")
public class ChangePasswordRequest {

    @XmlElementRef(name = "logonRequestData", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", type = JAXBElement.class, required = false)
    protected JAXBElement<LogonRequestData> logonRequestData;
    @XmlElementRef(name = "newPassword", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newPassword;

    /**
     * Gets the value of the logonRequestData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LogonRequestData }{@code >}
     *     
     */
    public JAXBElement<LogonRequestData> getLogonRequestData() {
        return logonRequestData;
    }

    /**
     * Sets the value of the logonRequestData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LogonRequestData }{@code >}
     *     
     */
    public void setLogonRequestData(JAXBElement<LogonRequestData> value) {
        this.logonRequestData = value;
    }

    /**
     * Gets the value of the newPassword property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewPassword() {
        return newPassword;
    }

    /**
     * Sets the value of the newPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewPassword(JAXBElement<String> value) {
        this.newPassword = value;
    }

}
