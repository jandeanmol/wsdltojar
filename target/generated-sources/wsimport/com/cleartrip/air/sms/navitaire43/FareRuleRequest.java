
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.navitaire.com/WebServices/DataContracts/Content}fareRuleReqData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fareRuleReqData"
})
@XmlRootElement(name = "FareRuleRequest", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/ContentService")
public class FareRuleRequest {

    @XmlElement(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", nillable = true)
    protected FareRuleRequestData fareRuleReqData;

    /**
     * Gets the value of the fareRuleReqData property.
     * 
     * @return
     *     possible object is
     *     {@link FareRuleRequestData }
     *     
     */
    public FareRuleRequestData getFareRuleReqData() {
        return fareRuleReqData;
    }

    /**
     * Sets the value of the fareRuleReqData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareRuleRequestData }
     *     
     */
    public void setFareRuleReqData(FareRuleRequestData value) {
        this.fareRuleReqData = value;
    }

}
