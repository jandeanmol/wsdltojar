
package com.cleartrip.air.sms.navitaire43;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for VoucherIssuance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoucherIssuance">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}ModifiedMessage">
 *       &lt;sequence>
 *         &lt;element name="VoucherBasisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherIssuanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="IssuanceReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InventorySegmentKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Vouchers" type="{http://schemas.navitaire.com/WebServices/DataContracts/Voucher}ArrayOfVoucher" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherIssuance", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", propOrder = {
    "voucherBasisCode",
    "voucherIssuanceID",
    "issuanceReasonCode",
    "note",
    "currencyCode",
    "amount",
    "expirationDate",
    "inventorySegmentKey",
    "vouchers"
})
public class VoucherIssuance
    extends ModifiedMessage
{

    @XmlElementRef(name = "VoucherBasisCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voucherBasisCode;
    @XmlElement(name = "VoucherIssuanceID")
    protected Long voucherIssuanceID;
    @XmlElementRef(name = "IssuanceReasonCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> issuanceReasonCode;
    @XmlElementRef(name = "Note", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> note;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "ExpirationDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlElementRef(name = "InventorySegmentKey", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> inventorySegmentKey;
    @XmlElementRef(name = "Vouchers", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfVoucher> vouchers;

    /**
     * Gets the value of the voucherBasisCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoucherBasisCode() {
        return voucherBasisCode;
    }

    /**
     * Sets the value of the voucherBasisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoucherBasisCode(JAXBElement<String> value) {
        this.voucherBasisCode = value;
    }

    /**
     * Gets the value of the voucherIssuanceID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVoucherIssuanceID() {
        return voucherIssuanceID;
    }

    /**
     * Sets the value of the voucherIssuanceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVoucherIssuanceID(Long value) {
        this.voucherIssuanceID = value;
    }

    /**
     * Gets the value of the issuanceReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIssuanceReasonCode() {
        return issuanceReasonCode;
    }

    /**
     * Sets the value of the issuanceReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIssuanceReasonCode(JAXBElement<String> value) {
        this.issuanceReasonCode = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNote(JAXBElement<String> value) {
        this.note = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the inventorySegmentKey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInventorySegmentKey() {
        return inventorySegmentKey;
    }

    /**
     * Sets the value of the inventorySegmentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInventorySegmentKey(JAXBElement<String> value) {
        this.inventorySegmentKey = value;
    }

    /**
     * Gets the value of the vouchers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVoucher }{@code >}
     *     
     */
    public JAXBElement<ArrayOfVoucher> getVouchers() {
        return vouchers;
    }

    /**
     * Sets the value of the vouchers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVoucher }{@code >}
     *     
     */
    public void setVouchers(JAXBElement<ArrayOfVoucher> value) {
        this.vouchers = value;
    }

}
