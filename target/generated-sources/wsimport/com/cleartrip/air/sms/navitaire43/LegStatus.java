
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LegStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Normal"/>
 *     &lt;enumeration value="Closed"/>
 *     &lt;enumeration value="Canceled"/>
 *     &lt;enumeration value="Suspended"/>
 *     &lt;enumeration value="ClosedPending"/>
 *     &lt;enumeration value="BlockAllActivities"/>
 *     &lt;enumeration value="Mishap"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LegStatus", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum LegStatus {

    @XmlEnumValue("Normal")
    NORMAL("Normal"),
    @XmlEnumValue("Closed")
    CLOSED("Closed"),
    @XmlEnumValue("Canceled")
    CANCELED("Canceled"),
    @XmlEnumValue("Suspended")
    SUSPENDED("Suspended"),
    @XmlEnumValue("ClosedPending")
    CLOSED_PENDING("ClosedPending"),
    @XmlEnumValue("BlockAllActivities")
    BLOCK_ALL_ACTIVITIES("BlockAllActivities"),
    @XmlEnumValue("Mishap")
    MISHAP("Mishap"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    LegStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LegStatus fromValue(String v) {
        for (LegStatus c: LegStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
