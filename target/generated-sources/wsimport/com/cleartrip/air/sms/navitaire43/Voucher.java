
package com.cleartrip.air.sms.navitaire43;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Voucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Voucher">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}ModifiedMessage">
 *       &lt;sequence>
 *         &lt;element name="VoucherID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="VoucherType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}VoucherType" minOccurs="0"/>
 *         &lt;element name="VoucherBasisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoucherIssuanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}VoucherStatus" minOccurs="0"/>
 *         &lt;element name="VoucherReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Available" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ForeignAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ForeignCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Expiration" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="RecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PersonID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="VoucherTransactions" type="{http://schemas.navitaire.com/WebServices/DataContracts/Voucher}ArrayOfVoucherTransaction" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Voucher", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", propOrder = {
    "voucherID",
    "voucherType",
    "voucherBasisCode",
    "voucherIssuanceID",
    "status",
    "voucherReference",
    "password",
    "amount",
    "available",
    "currencyCode",
    "foreignAmount",
    "foreignCurrencyCode",
    "expiration",
    "recordLocator",
    "lastName",
    "firstName",
    "personID",
    "voucherTransactions"
})
public class Voucher
    extends ModifiedMessage
{

    @XmlElement(name = "VoucherID")
    protected Long voucherID;
    @XmlElement(name = "VoucherType")
    protected VoucherType voucherType;
    @XmlElementRef(name = "VoucherBasisCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voucherBasisCode;
    @XmlElement(name = "VoucherIssuanceID")
    protected Long voucherIssuanceID;
    @XmlElement(name = "Status")
    protected VoucherStatus status;
    @XmlElementRef(name = "VoucherReference", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> voucherReference;
    @XmlElementRef(name = "Password", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> password;
    @XmlElement(name = "Amount")
    protected BigDecimal amount;
    @XmlElement(name = "Available")
    protected BigDecimal available;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElement(name = "ForeignAmount")
    protected BigDecimal foreignAmount;
    @XmlElementRef(name = "ForeignCurrencyCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> foreignCurrencyCode;
    @XmlElement(name = "Expiration")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expiration;
    @XmlElementRef(name = "RecordLocator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recordLocator;
    @XmlElementRef(name = "LastName", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastName;
    @XmlElementRef(name = "FirstName", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<String> firstName;
    @XmlElement(name = "PersonID")
    protected Long personID;
    @XmlElementRef(name = "VoucherTransactions", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Voucher", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfVoucherTransaction> voucherTransactions;

    /**
     * Gets the value of the voucherID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVoucherID() {
        return voucherID;
    }

    /**
     * Sets the value of the voucherID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVoucherID(Long value) {
        this.voucherID = value;
    }

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherType }
     *     
     */
    public VoucherType getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherType }
     *     
     */
    public void setVoucherType(VoucherType value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the voucherBasisCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoucherBasisCode() {
        return voucherBasisCode;
    }

    /**
     * Sets the value of the voucherBasisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoucherBasisCode(JAXBElement<String> value) {
        this.voucherBasisCode = value;
    }

    /**
     * Gets the value of the voucherIssuanceID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getVoucherIssuanceID() {
        return voucherIssuanceID;
    }

    /**
     * Sets the value of the voucherIssuanceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setVoucherIssuanceID(Long value) {
        this.voucherIssuanceID = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherStatus }
     *     
     */
    public VoucherStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherStatus }
     *     
     */
    public void setStatus(VoucherStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the voucherReference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVoucherReference() {
        return voucherReference;
    }

    /**
     * Sets the value of the voucherReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVoucherReference(JAXBElement<String> value) {
        this.voucherReference = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPassword(JAXBElement<String> value) {
        this.password = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the available property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAvailable() {
        return available;
    }

    /**
     * Sets the value of the available property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAvailable(BigDecimal value) {
        this.available = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the foreignAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getForeignAmount() {
        return foreignAmount;
    }

    /**
     * Sets the value of the foreignAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setForeignAmount(BigDecimal value) {
        this.foreignAmount = value;
    }

    /**
     * Gets the value of the foreignCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForeignCurrencyCode() {
        return foreignCurrencyCode;
    }

    /**
     * Sets the value of the foreignCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForeignCurrencyCode(JAXBElement<String> value) {
        this.foreignCurrencyCode = value;
    }

    /**
     * Gets the value of the expiration property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpiration() {
        return expiration;
    }

    /**
     * Sets the value of the expiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpiration(XMLGregorianCalendar value) {
        this.expiration = value;
    }

    /**
     * Gets the value of the recordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecordLocator() {
        return recordLocator;
    }

    /**
     * Sets the value of the recordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecordLocator(JAXBElement<String> value) {
        this.recordLocator = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastName(JAXBElement<String> value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFirstName(JAXBElement<String> value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the personID property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPersonID() {
        return personID;
    }

    /**
     * Sets the value of the personID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPersonID(Long value) {
        this.personID = value;
    }

    /**
     * Gets the value of the voucherTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVoucherTransaction }{@code >}
     *     
     */
    public JAXBElement<ArrayOfVoucherTransaction> getVoucherTransactions() {
        return voucherTransactions;
    }

    /**
     * Sets the value of the voucherTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVoucherTransaction }{@code >}
     *     
     */
    public void setVoucherTransactions(JAXBElement<ArrayOfVoucherTransaction> value) {
        this.voucherTransactions = value;
    }

}
