
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://schemas.navitaire.com/WebServices/DataContracts/Content}contentReqData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contentReqData"
})
@XmlRootElement(name = "CommitContentRequest", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/ContentService")
public class CommitContentRequest {

    @XmlElement(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Content", nillable = true)
    protected Content contentReqData;

    /**
     * Gets the value of the contentReqData property.
     * 
     * @return
     *     possible object is
     *     {@link Content }
     *     
     */
    public Content getContentReqData() {
        return contentReqData;
    }

    /**
     * Sets the value of the contentReqData property.
     * 
     * @param value
     *     allowed object is
     *     {@link Content }
     *     
     */
    public void setContentReqData(Content value) {
        this.contentReqData = value;
    }

}
