
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StateMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StateMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="State" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}MessageState" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StateMessage", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", propOrder = {
    "state"
})
@XmlSeeAlso({
    Segment.class,
    PaxBag.class,
    PaxSeat.class,
    PaxSSR.class,
    PaxTicket.class,
    PaxFare.class,
    LegNest.class,
    PaxSegment.class,
    Fare.class,
    Leg.class,
    OperationsInfo.class,
    BookingServiceCharge.class,
    Journey.class,
    LegInfo.class,
    LegClass.class,
    ModifiedMessage.class,
    PointOfSale.class
})
public class StateMessage {

    @XmlElement(name = "State")
    protected MessageState state;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link MessageState }
     *     
     */
    public MessageState getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageState }
     *     
     */
    public void setState(MessageState value) {
        this.state = value;
    }

}
