
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Fare complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Fare">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="ClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RuleTariff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CarrierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RuleNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareBasisCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareSequence" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="FareClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareStatus" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}FareStatus" minOccurs="0"/>
 *         &lt;element name="FareApplicationType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}FareApplicationType" minOccurs="0"/>
 *         &lt;element name="OriginalClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XrefClassOfService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaxFares" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfPaxFare" minOccurs="0"/>
 *         &lt;element name="ProductClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAllotmentMarketFare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TravelClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareSellKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InboundOutbound" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}InboundOutbound" minOccurs="0"/>
 *         &lt;element name="FareLink" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="FareDesignator" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}FareDesignator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Fare", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "classOfService",
    "classType",
    "ruleTariff",
    "carrierCode",
    "ruleNumber",
    "fareBasisCode",
    "fareSequence",
    "fareClassOfService",
    "fareStatus",
    "fareApplicationType",
    "originalClassOfService",
    "xrefClassOfService",
    "paxFares",
    "productClass",
    "isAllotmentMarketFare",
    "travelClassCode",
    "fareSellKey",
    "inboundOutbound",
    "fareLink",
    "fareDesignator"
})
@XmlSeeAlso({
    AvailableFare.class
})
public class Fare
    extends StateMessage
{

    @XmlElementRef(name = "ClassOfService", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> classOfService;
    @XmlElementRef(name = "ClassType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> classType;
    @XmlElementRef(name = "RuleTariff", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ruleTariff;
    @XmlElementRef(name = "CarrierCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> carrierCode;
    @XmlElementRef(name = "RuleNumber", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ruleNumber;
    @XmlElementRef(name = "FareBasisCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fareBasisCode;
    @XmlElement(name = "FareSequence")
    protected Short fareSequence;
    @XmlElementRef(name = "FareClassOfService", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fareClassOfService;
    @XmlElement(name = "FareStatus")
    protected FareStatus fareStatus;
    @XmlElement(name = "FareApplicationType")
    protected FareApplicationType fareApplicationType;
    @XmlElementRef(name = "OriginalClassOfService", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> originalClassOfService;
    @XmlElementRef(name = "XrefClassOfService", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> xrefClassOfService;
    @XmlElementRef(name = "PaxFares", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPaxFare> paxFares;
    @XmlElementRef(name = "ProductClass", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productClass;
    @XmlElement(name = "IsAllotmentMarketFare")
    protected Boolean isAllotmentMarketFare;
    @XmlElementRef(name = "TravelClassCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> travelClassCode;
    @XmlElementRef(name = "FareSellKey", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fareSellKey;
    @XmlElement(name = "InboundOutbound")
    protected InboundOutbound inboundOutbound;
    @XmlElement(name = "FareLink")
    protected Short fareLink;
    @XmlElementRef(name = "FareDesignator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<FareDesignator> fareDesignator;

    /**
     * Gets the value of the classOfService property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClassOfService() {
        return classOfService;
    }

    /**
     * Sets the value of the classOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClassOfService(JAXBElement<String> value) {
        this.classOfService = value;
    }

    /**
     * Gets the value of the classType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClassType() {
        return classType;
    }

    /**
     * Sets the value of the classType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClassType(JAXBElement<String> value) {
        this.classType = value;
    }

    /**
     * Gets the value of the ruleTariff property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRuleTariff() {
        return ruleTariff;
    }

    /**
     * Sets the value of the ruleTariff property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRuleTariff(JAXBElement<String> value) {
        this.ruleTariff = value;
    }

    /**
     * Gets the value of the carrierCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCarrierCode() {
        return carrierCode;
    }

    /**
     * Sets the value of the carrierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCarrierCode(JAXBElement<String> value) {
        this.carrierCode = value;
    }

    /**
     * Gets the value of the ruleNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRuleNumber() {
        return ruleNumber;
    }

    /**
     * Sets the value of the ruleNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRuleNumber(JAXBElement<String> value) {
        this.ruleNumber = value;
    }

    /**
     * Gets the value of the fareBasisCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFareBasisCode() {
        return fareBasisCode;
    }

    /**
     * Sets the value of the fareBasisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFareBasisCode(JAXBElement<String> value) {
        this.fareBasisCode = value;
    }

    /**
     * Gets the value of the fareSequence property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getFareSequence() {
        return fareSequence;
    }

    /**
     * Sets the value of the fareSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setFareSequence(Short value) {
        this.fareSequence = value;
    }

    /**
     * Gets the value of the fareClassOfService property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFareClassOfService() {
        return fareClassOfService;
    }

    /**
     * Sets the value of the fareClassOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFareClassOfService(JAXBElement<String> value) {
        this.fareClassOfService = value;
    }

    /**
     * Gets the value of the fareStatus property.
     * 
     * @return
     *     possible object is
     *     {@link FareStatus }
     *     
     */
    public FareStatus getFareStatus() {
        return fareStatus;
    }

    /**
     * Sets the value of the fareStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareStatus }
     *     
     */
    public void setFareStatus(FareStatus value) {
        this.fareStatus = value;
    }

    /**
     * Gets the value of the fareApplicationType property.
     * 
     * @return
     *     possible object is
     *     {@link FareApplicationType }
     *     
     */
    public FareApplicationType getFareApplicationType() {
        return fareApplicationType;
    }

    /**
     * Sets the value of the fareApplicationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareApplicationType }
     *     
     */
    public void setFareApplicationType(FareApplicationType value) {
        this.fareApplicationType = value;
    }

    /**
     * Gets the value of the originalClassOfService property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOriginalClassOfService() {
        return originalClassOfService;
    }

    /**
     * Sets the value of the originalClassOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOriginalClassOfService(JAXBElement<String> value) {
        this.originalClassOfService = value;
    }

    /**
     * Gets the value of the xrefClassOfService property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getXrefClassOfService() {
        return xrefClassOfService;
    }

    /**
     * Sets the value of the xrefClassOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setXrefClassOfService(JAXBElement<String> value) {
        this.xrefClassOfService = value;
    }

    /**
     * Gets the value of the paxFares property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxFare }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPaxFare> getPaxFares() {
        return paxFares;
    }

    /**
     * Sets the value of the paxFares property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPaxFare }{@code >}
     *     
     */
    public void setPaxFares(JAXBElement<ArrayOfPaxFare> value) {
        this.paxFares = value;
    }

    /**
     * Gets the value of the productClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductClass() {
        return productClass;
    }

    /**
     * Sets the value of the productClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductClass(JAXBElement<String> value) {
        this.productClass = value;
    }

    /**
     * Gets the value of the isAllotmentMarketFare property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAllotmentMarketFare() {
        return isAllotmentMarketFare;
    }

    /**
     * Sets the value of the isAllotmentMarketFare property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAllotmentMarketFare(Boolean value) {
        this.isAllotmentMarketFare = value;
    }

    /**
     * Gets the value of the travelClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTravelClassCode() {
        return travelClassCode;
    }

    /**
     * Sets the value of the travelClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTravelClassCode(JAXBElement<String> value) {
        this.travelClassCode = value;
    }

    /**
     * Gets the value of the fareSellKey property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFareSellKey() {
        return fareSellKey;
    }

    /**
     * Sets the value of the fareSellKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFareSellKey(JAXBElement<String> value) {
        this.fareSellKey = value;
    }

    /**
     * Gets the value of the inboundOutbound property.
     * 
     * @return
     *     possible object is
     *     {@link InboundOutbound }
     *     
     */
    public InboundOutbound getInboundOutbound() {
        return inboundOutbound;
    }

    /**
     * Sets the value of the inboundOutbound property.
     * 
     * @param value
     *     allowed object is
     *     {@link InboundOutbound }
     *     
     */
    public void setInboundOutbound(InboundOutbound value) {
        this.inboundOutbound = value;
    }

    /**
     * Gets the value of the fareLink property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getFareLink() {
        return fareLink;
    }

    /**
     * Sets the value of the fareLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setFareLink(Short value) {
        this.fareLink = value;
    }

    /**
     * Gets the value of the fareDesignator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FareDesignator }{@code >}
     *     
     */
    public JAXBElement<FareDesignator> getFareDesignator() {
        return fareDesignator;
    }

    /**
     * Sets the value of the fareDesignator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FareDesignator }{@code >}
     *     
     */
    public void setFareDesignator(JAXBElement<FareDesignator> value) {
        this.fareDesignator = value;
    }

}
