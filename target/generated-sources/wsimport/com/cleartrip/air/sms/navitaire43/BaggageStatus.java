
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BaggageStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BaggageStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="Checked"/>
 *     &lt;enumeration value="Removed"/>
 *     &lt;enumeration value="Added"/>
 *     &lt;enumeration value="AddedPrinted"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BaggageStatus", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum BaggageStatus {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Checked")
    CHECKED("Checked"),
    @XmlEnumValue("Removed")
    REMOVED("Removed"),
    @XmlEnumValue("Added")
    ADDED("Added"),
    @XmlEnumValue("AddedPrinted")
    ADDED_PRINTED("AddedPrinted"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    BaggageStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BaggageStatus fromValue(String v) {
        for (BaggageStatus c: BaggageStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
