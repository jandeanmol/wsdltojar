
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LegNest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegNest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="ClassNest" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="Lid" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="AdjustedCapacity" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="TravelClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NestType" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}NestType" minOccurs="0"/>
 *         &lt;element name="LegClasses" type="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}ArrayOfLegClass" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegNest", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "classNest",
    "lid",
    "adjustedCapacity",
    "travelClassCode",
    "nestType",
    "legClasses"
})
public class LegNest
    extends StateMessage
{

    @XmlElement(name = "ClassNest")
    protected Short classNest;
    @XmlElement(name = "Lid")
    protected Short lid;
    @XmlElement(name = "AdjustedCapacity")
    protected Short adjustedCapacity;
    @XmlElementRef(name = "TravelClassCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> travelClassCode;
    @XmlElement(name = "NestType")
    protected NestType nestType;
    @XmlElementRef(name = "LegClasses", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLegClass> legClasses;

    /**
     * Gets the value of the classNest property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getClassNest() {
        return classNest;
    }

    /**
     * Sets the value of the classNest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setClassNest(Short value) {
        this.classNest = value;
    }

    /**
     * Gets the value of the lid property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getLid() {
        return lid;
    }

    /**
     * Sets the value of the lid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setLid(Short value) {
        this.lid = value;
    }

    /**
     * Gets the value of the adjustedCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAdjustedCapacity() {
        return adjustedCapacity;
    }

    /**
     * Sets the value of the adjustedCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAdjustedCapacity(Short value) {
        this.adjustedCapacity = value;
    }

    /**
     * Gets the value of the travelClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTravelClassCode() {
        return travelClassCode;
    }

    /**
     * Sets the value of the travelClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTravelClassCode(JAXBElement<String> value) {
        this.travelClassCode = value;
    }

    /**
     * Gets the value of the nestType property.
     * 
     * @return
     *     possible object is
     *     {@link NestType }
     *     
     */
    public NestType getNestType() {
        return nestType;
    }

    /**
     * Sets the value of the nestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link NestType }
     *     
     */
    public void setNestType(NestType value) {
        this.nestType = value;
    }

    /**
     * Gets the value of the legClasses property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLegClass }{@code >}
     *     
     */
    public JAXBElement<ArrayOfLegClass> getLegClasses() {
        return legClasses;
    }

    /**
     * Sets the value of the legClasses property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLegClass }{@code >}
     *     
     */
    public void setLegClasses(JAXBElement<ArrayOfLegClass> value) {
        this.legClasses = value;
    }

}
