
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FareDesignator complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FareDesignator">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FareTypeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TravelCityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RuleFareTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseFareFareClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DowType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SeasonType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RoutingNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="OneWayRoundTrip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OpenJawAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TripDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FareByRuleAccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareDesignator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "fareTypeIndicator",
    "cityCode",
    "travelCityCode",
    "ruleFareTypeCode",
    "baseFareFareClassCode",
    "dowType",
    "seasonType",
    "routingNumber",
    "oneWayRoundTrip",
    "openJawAllowed",
    "tripDirection",
    "fareByRuleAccountCode"
})
public class FareDesignator {

    @XmlElementRef(name = "FareTypeIndicator", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fareTypeIndicator;
    @XmlElementRef(name = "CityCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cityCode;
    @XmlElementRef(name = "TravelCityCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> travelCityCode;
    @XmlElementRef(name = "RuleFareTypeCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ruleFareTypeCode;
    @XmlElementRef(name = "BaseFareFareClassCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseFareFareClassCode;
    @XmlElementRef(name = "DowType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dowType;
    @XmlElementRef(name = "SeasonType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> seasonType;
    @XmlElement(name = "RoutingNumber")
    protected Short routingNumber;
    @XmlElementRef(name = "OneWayRoundTrip", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> oneWayRoundTrip;
    @XmlElement(name = "OpenJawAllowed")
    protected Boolean openJawAllowed;
    @XmlElementRef(name = "TripDirection", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tripDirection;
    @XmlElementRef(name = "FareByRuleAccountCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fareByRuleAccountCode;

    /**
     * Gets the value of the fareTypeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFareTypeIndicator() {
        return fareTypeIndicator;
    }

    /**
     * Sets the value of the fareTypeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFareTypeIndicator(JAXBElement<String> value) {
        this.fareTypeIndicator = value;
    }

    /**
     * Gets the value of the cityCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCityCode() {
        return cityCode;
    }

    /**
     * Sets the value of the cityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCityCode(JAXBElement<String> value) {
        this.cityCode = value;
    }

    /**
     * Gets the value of the travelCityCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTravelCityCode() {
        return travelCityCode;
    }

    /**
     * Sets the value of the travelCityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTravelCityCode(JAXBElement<String> value) {
        this.travelCityCode = value;
    }

    /**
     * Gets the value of the ruleFareTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRuleFareTypeCode() {
        return ruleFareTypeCode;
    }

    /**
     * Sets the value of the ruleFareTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRuleFareTypeCode(JAXBElement<String> value) {
        this.ruleFareTypeCode = value;
    }

    /**
     * Gets the value of the baseFareFareClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseFareFareClassCode() {
        return baseFareFareClassCode;
    }

    /**
     * Sets the value of the baseFareFareClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseFareFareClassCode(JAXBElement<String> value) {
        this.baseFareFareClassCode = value;
    }

    /**
     * Gets the value of the dowType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDowType() {
        return dowType;
    }

    /**
     * Sets the value of the dowType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDowType(JAXBElement<String> value) {
        this.dowType = value;
    }

    /**
     * Gets the value of the seasonType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeasonType() {
        return seasonType;
    }

    /**
     * Sets the value of the seasonType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeasonType(JAXBElement<String> value) {
        this.seasonType = value;
    }

    /**
     * Gets the value of the routingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getRoutingNumber() {
        return routingNumber;
    }

    /**
     * Sets the value of the routingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setRoutingNumber(Short value) {
        this.routingNumber = value;
    }

    /**
     * Gets the value of the oneWayRoundTrip property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOneWayRoundTrip() {
        return oneWayRoundTrip;
    }

    /**
     * Sets the value of the oneWayRoundTrip property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOneWayRoundTrip(JAXBElement<String> value) {
        this.oneWayRoundTrip = value;
    }

    /**
     * Gets the value of the openJawAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOpenJawAllowed() {
        return openJawAllowed;
    }

    /**
     * Sets the value of the openJawAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOpenJawAllowed(Boolean value) {
        this.openJawAllowed = value;
    }

    /**
     * Gets the value of the tripDirection property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTripDirection() {
        return tripDirection;
    }

    /**
     * Sets the value of the tripDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTripDirection(JAXBElement<String> value) {
        this.tripDirection = value;
    }

    /**
     * Gets the value of the fareByRuleAccountCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFareByRuleAccountCode() {
        return fareByRuleAccountCode;
    }

    /**
     * Sets the value of the fareByRuleAccountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFareByRuleAccountCode(JAXBElement<String> value) {
        this.fareByRuleAccountCode = value;
    }

}
