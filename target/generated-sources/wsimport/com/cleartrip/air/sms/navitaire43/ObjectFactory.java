
package com.cleartrip.air.sms.navitaire43;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cleartrip.air.sms.navitaire43 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SystemType_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "SystemType");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _MinInclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "minInclusive");
    private final static QName _AttributeGroup_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "attributeGroup");
    private final static QName _MinLength_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "minLength");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Enumeration_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "enumeration");
    private final static QName _Group_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "group");
    private final static QName _Element_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "element");
    private final static QName _TransferSessionResponseData_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "TransferSessionResponseData");
    private final static QName _MaxInclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "maxInclusive");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _MinExclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "minExclusive");
    private final static QName _AnyAttribute_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "anyAttribute");
    private final static QName _APIUnhandledServerFault_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "APIUnhandledServerFault");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _MaxExclusive_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "maxExclusive");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _SimpleType_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "simpleType");
    private final static QName _All_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "all");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _ValidationResult_QNAME = new QName("http://www.navitaire.com/Ncl/Validation/ValidationResult", "ValidationResult");
    private final static QName _APICriticalFault_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "APICriticalFault");
    private final static QName _ComplexType_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "complexType");
    private final static QName _APIValidationFault_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "APIValidationFault");
    private final static QName _APISecurityFault_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "APISecurityFault");
    private final static QName _Dictionary_QNAME = new QName("", "dictionary");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _MaxLength_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "maxLength");
    private final static QName _Sequence_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "sequence");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _APIFault_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "APIFault");
    private final static QName _APIGeneralFault_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "APIGeneralFault");
    private final static QName _Unique_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "unique");
    private final static QName _EnableExceptionStackTrace_QNAME = new QName("http://schemas.navitaire.com/WebServices", "EnableExceptionStackTrace");
    private final static QName _LogonRequestData_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "LogonRequestData");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _ArrayOfValidationResult_QNAME = new QName("http://www.navitaire.com/Ncl/Validation/ValidationResult", "ArrayOfValidationResult");
    private final static QName _TokenRequest_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "TokenRequest");
    private final static QName _Attribute_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "attribute");
    private final static QName _ContractVersion_QNAME = new QName("http://schemas.navitaire.com/WebServices", "ContractVersion");
    private final static QName _APIWarningFault_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "APIWarningFault");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Key_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "key");
    private final static QName _Signature_QNAME = new QName("http://schemas.navitaire.com/WebServices", "Signature");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _Length_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "length");
    private final static QName _FractionDigits_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "fractionDigits");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _MessageContractVersion_QNAME = new QName("http://schemas.navitaire.com/WebServices", "MessageContractVersion");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _ChannelType_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", "ChannelType");
    private final static QName _Choice_QNAME = new QName("http://www.w3.org/2001/XMLSchema", "choice");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _WhoAmIResponseData_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "WhoAmIResponseData");
    private final static QName _ChangePasswordRequestLogonRequestData_QNAME = new QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", "logonRequestData");
    private final static QName _ChangePasswordRequestNewPassword_QNAME = new QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", "newPassword");
    private final static QName _TransferSessionRequestTokenRequest_QNAME = new QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", "tokenRequest");
    private final static QName _ValidationResultAnnotations_QNAME = new QName("http://www.navitaire.com/Ncl/Validation/ValidationResult", "Annotations");
    private final static QName _ValidationResultKey_QNAME = new QName("http://www.navitaire.com/Ncl/Validation/ValidationResult", "Key");
    private final static QName _ValidationResultFailedValidationDescription_QNAME = new QName("http://www.navitaire.com/Ncl/Validation/ValidationResult", "FailedValidationDescription");
    private final static QName _TransferSessionResponseDataSignature_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "Signature");
    private final static QName _APIFaultErrorCode_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "ErrorCode");
    private final static QName _APIFaultErrorType_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "ErrorType");
    private final static QName _APIFaultMessage_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "Message");
    private final static QName _APIFaultStackTrace_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "StackTrace");
    private final static QName _LogonRequestDataDomainCode_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "DomainCode");
    private final static QName _LogonRequestDataClientName_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "ClientName");
    private final static QName _LogonRequestDataPassword_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "Password");
    private final static QName _LogonRequestDataRoleCode_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "RoleCode");
    private final static QName _LogonRequestDataAgentName_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "AgentName");
    private final static QName _LogonRequestDataTerminalInfo_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "TerminalInfo");
    private final static QName _LogonRequestDataLocationCode_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "LocationCode");
    private final static QName _TokenRequestToken_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "Token");
    private final static QName _TransferSessionResponseTransferSessionResponseData_QNAME = new QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", "TransferSessionResponseData");
    private final static QName _APIValidationFaultValidationResults_QNAME = new QName("http://schemas.navitaire.com/WebServices/FaultContracts", "ValidationResults");
    private final static QName _WhoAmIResponseWhoAmIResponseData_QNAME = new QName("http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", "WhoAmIResponseData");
    private final static QName _WhoAmIResponseDataOrganizationCode_QNAME = new QName("http://schemas.navitaire.com/WebServices/DataContracts/Session", "OrganizationCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cleartrip.air.sms.navitaire43
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Schema }
     * 
     */
    public Schema createSchema() {
        return new Schema();
    }

    /**
     * Create an instance of {@link OpenAttrs }
     * 
     */
    public OpenAttrs createOpenAttrs() {
        return new OpenAttrs();
    }

    /**
     * Create an instance of {@link Include }
     * 
     */
    public Include createInclude() {
        return new Include();
    }

    /**
     * Create an instance of {@link Annotated }
     * 
     */
    public Annotated createAnnotated() {
        return new Annotated();
    }

    /**
     * Create an instance of {@link Annotation }
     * 
     */
    public Annotation createAnnotation() {
        return new Annotation();
    }

    /**
     * Create an instance of {@link Appinfo }
     * 
     */
    public Appinfo createAppinfo() {
        return new Appinfo();
    }

    /**
     * Create an instance of {@link Documentation }
     * 
     */
    public Documentation createDocumentation() {
        return new Documentation();
    }

    /**
     * Create an instance of {@link Import }
     * 
     */
    public Import createImport() {
        return new Import();
    }

    /**
     * Create an instance of {@link Redefine }
     * 
     */
    public Redefine createRedefine() {
        return new Redefine();
    }

    /**
     * Create an instance of {@link TopLevelSimpleType }
     * 
     */
    public TopLevelSimpleType createTopLevelSimpleType() {
        return new TopLevelSimpleType();
    }

    /**
     * Create an instance of {@link TopLevelComplexType }
     * 
     */
    public TopLevelComplexType createTopLevelComplexType() {
        return new TopLevelComplexType();
    }

    /**
     * Create an instance of {@link NamedGroup }
     * 
     */
    public NamedGroup createNamedGroup() {
        return new NamedGroup();
    }

    /**
     * Create an instance of {@link NamedAttributeGroup }
     * 
     */
    public NamedAttributeGroup createNamedAttributeGroup() {
        return new NamedAttributeGroup();
    }

    /**
     * Create an instance of {@link TopLevelElement }
     * 
     */
    public TopLevelElement createTopLevelElement() {
        return new TopLevelElement();
    }

    /**
     * Create an instance of {@link TopLevelAttribute }
     * 
     */
    public TopLevelAttribute createTopLevelAttribute() {
        return new TopLevelAttribute();
    }

    /**
     * Create an instance of {@link Notation }
     * 
     */
    public Notation createNotation() {
        return new Notation();
    }

    /**
     * Create an instance of {@link Facet }
     * 
     */
    public Facet createFacet() {
        return new Facet();
    }

    /**
     * Create an instance of {@link NumFacet }
     * 
     */
    public NumFacet createNumFacet() {
        return new NumFacet();
    }

    /**
     * Create an instance of {@link Pattern }
     * 
     */
    public Pattern createPattern() {
        return new Pattern();
    }

    /**
     * Create an instance of {@link NoFixedFacet }
     * 
     */
    public NoFixedFacet createNoFixedFacet() {
        return new NoFixedFacet();
    }

    /**
     * Create an instance of {@link Keyref }
     * 
     */
    public Keyref createKeyref() {
        return new Keyref();
    }

    /**
     * Create an instance of {@link Keybase }
     * 
     */
    public Keybase createKeybase() {
        return new Keybase();
    }

    /**
     * Create an instance of {@link Selector }
     * 
     */
    public Selector createSelector() {
        return new Selector();
    }

    /**
     * Create an instance of {@link Field }
     * 
     */
    public Field createField() {
        return new Field();
    }

    /**
     * Create an instance of {@link Wildcard }
     * 
     */
    public Wildcard createWildcard() {
        return new Wildcard();
    }

    /**
     * Create an instance of {@link ComplexContent }
     * 
     */
    public ComplexContent createComplexContent() {
        return new ComplexContent();
    }

    /**
     * Create an instance of {@link ComplexRestrictionType }
     * 
     */
    public ComplexRestrictionType createComplexRestrictionType() {
        return new ComplexRestrictionType();
    }

    /**
     * Create an instance of {@link ExtensionType }
     * 
     */
    public ExtensionType createExtensionType() {
        return new ExtensionType();
    }

    /**
     * Create an instance of {@link All }
     * 
     */
    public All createAll() {
        return new All();
    }

    /**
     * Create an instance of {@link WhiteSpace }
     * 
     */
    public WhiteSpace createWhiteSpace() {
        return new WhiteSpace();
    }

    /**
     * Create an instance of {@link TotalDigits }
     * 
     */
    public TotalDigits createTotalDigits() {
        return new TotalDigits();
    }

    /**
     * Create an instance of {@link Union }
     * 
     */
    public Union createUnion() {
        return new Union();
    }

    /**
     * Create an instance of {@link LocalSimpleType }
     * 
     */
    public LocalSimpleType createLocalSimpleType() {
        return new LocalSimpleType();
    }

    /**
     * Create an instance of {@link List }
     * 
     */
    public List createList() {
        return new List();
    }

    /**
     * Create an instance of {@link Any }
     * 
     */
    public Any createAny() {
        return new Any();
    }

    /**
     * Create an instance of {@link ExplicitGroup }
     * 
     */
    public ExplicitGroup createExplicitGroup() {
        return new ExplicitGroup();
    }

    /**
     * Create an instance of {@link SimpleContent }
     * 
     */
    public SimpleContent createSimpleContent() {
        return new SimpleContent();
    }

    /**
     * Create an instance of {@link SimpleRestrictionType }
     * 
     */
    public SimpleRestrictionType createSimpleRestrictionType() {
        return new SimpleRestrictionType();
    }

    /**
     * Create an instance of {@link SimpleExtensionType }
     * 
     */
    public SimpleExtensionType createSimpleExtensionType() {
        return new SimpleExtensionType();
    }

    /**
     * Create an instance of {@link Restriction }
     * 
     */
    public Restriction createRestriction() {
        return new Restriction();
    }

    /**
     * Create an instance of {@link LocalElement }
     * 
     */
    public LocalElement createLocalElement() {
        return new LocalElement();
    }

    /**
     * Create an instance of {@link RestrictionType }
     * 
     */
    public RestrictionType createRestrictionType() {
        return new RestrictionType();
    }

    /**
     * Create an instance of {@link GroupRef }
     * 
     */
    public GroupRef createGroupRef() {
        return new GroupRef();
    }

    /**
     * Create an instance of {@link LocalComplexType }
     * 
     */
    public LocalComplexType createLocalComplexType() {
        return new LocalComplexType();
    }

    /**
     * Create an instance of {@link SimpleExplicitGroup }
     * 
     */
    public SimpleExplicitGroup createSimpleExplicitGroup() {
        return new SimpleExplicitGroup();
    }

    /**
     * Create an instance of {@link NarrowMaxMin }
     * 
     */
    public NarrowMaxMin createNarrowMaxMin() {
        return new NarrowMaxMin();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link AttributeGroupRef }
     * 
     */
    public AttributeGroupRef createAttributeGroupRef() {
        return new AttributeGroupRef();
    }

    /**
     * Create an instance of {@link RealGroup }
     * 
     */
    public RealGroup createRealGroup() {
        return new RealGroup();
    }

    /**
     * Create an instance of {@link SerializableDictionaryOfstringstring }
     * 
     */
    public SerializableDictionaryOfstringstring createSerializableDictionaryOfstringstring() {
        return new SerializableDictionaryOfstringstring();
    }

    /**
     * Create an instance of {@link TransferSessionResponseData }
     * 
     */
    public TransferSessionResponseData createTransferSessionResponseData() {
        return new TransferSessionResponseData();
    }

    /**
     * Create an instance of {@link LogonRequestData }
     * 
     */
    public LogonRequestData createLogonRequestData() {
        return new LogonRequestData();
    }

    /**
     * Create an instance of {@link TokenRequest }
     * 
     */
    public TokenRequest createTokenRequest() {
        return new TokenRequest();
    }

    /**
     * Create an instance of {@link WhoAmIResponseData }
     * 
     */
    public WhoAmIResponseData createWhoAmIResponseData() {
        return new WhoAmIResponseData();
    }

    /**
     * Create an instance of {@link WhoAmIRequest }
     * 
     */
    public WhoAmIRequest createWhoAmIRequest() {
        return new WhoAmIRequest();
    }

    /**
     * Create an instance of {@link TransferSessionRequest }
     * 
     */
    public TransferSessionRequest createTransferSessionRequest() {
        return new TransferSessionRequest();
    }

    /**
     * Create an instance of {@link LogonRequest }
     * 
     */
    public LogonRequest createLogonRequest() {
        return new LogonRequest();
    }

    /**
     * Create an instance of {@link KeepAliveRequest }
     * 
     */
    public KeepAliveRequest createKeepAliveRequest() {
        return new KeepAliveRequest();
    }

    /**
     * Create an instance of {@link LogoutRequest }
     * 
     */
    public LogoutRequest createLogoutRequest() {
        return new LogoutRequest();
    }

    /**
     * Create an instance of {@link WhoAmIResponse }
     * 
     */
    public WhoAmIResponse createWhoAmIResponse() {
        return new WhoAmIResponse();
    }

    /**
     * Create an instance of {@link TransferSessionResponse }
     * 
     */
    public TransferSessionResponse createTransferSessionResponse() {
        return new TransferSessionResponse();
    }

    /**
     * Create an instance of {@link ChangePasswordRequest }
     * 
     */
    public ChangePasswordRequest createChangePasswordRequest() {
        return new ChangePasswordRequest();
    }

    /**
     * Create an instance of {@link ArrayOfValidationResult }
     * 
     */
    public ArrayOfValidationResult createArrayOfValidationResult() {
        return new ArrayOfValidationResult();
    }

    /**
     * Create an instance of {@link ValidationResult }
     * 
     */
    public ValidationResult createValidationResult() {
        return new ValidationResult();
    }

    /**
     * Create an instance of {@link LogonResponse }
     * 
     */
    public LogonResponse createLogonResponse() {
        return new LogonResponse();
    }

    /**
     * Create an instance of {@link APIFault }
     * 
     */
    public APIFault createAPIFault() {
        return new APIFault();
    }

    /**
     * Create an instance of {@link APICriticalFault }
     * 
     */
    public APICriticalFault createAPICriticalFault() {
        return new APICriticalFault();
    }

    /**
     * Create an instance of {@link APIGeneralFault }
     * 
     */
    public APIGeneralFault createAPIGeneralFault() {
        return new APIGeneralFault();
    }

    /**
     * Create an instance of {@link APIUnhandledServerFault }
     * 
     */
    public APIUnhandledServerFault createAPIUnhandledServerFault() {
        return new APIUnhandledServerFault();
    }

    /**
     * Create an instance of {@link APIWarningFault }
     * 
     */
    public APIWarningFault createAPIWarningFault() {
        return new APIWarningFault();
    }

    /**
     * Create an instance of {@link APIValidationFault }
     * 
     */
    public APIValidationFault createAPIValidationFault() {
        return new APIValidationFault();
    }

    /**
     * Create an instance of {@link APISecurityFault }
     * 
     */
    public APISecurityFault createAPISecurityFault() {
        return new APISecurityFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SystemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", name = "SystemType")
    public JAXBElement<SystemType> createSystemType(SystemType value) {
        return new JAXBElement<SystemType>(_SystemType_QNAME, SystemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "minInclusive")
    public JAXBElement<Facet> createMinInclusive(Facet value) {
        return new JAXBElement<Facet>(_MinInclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NamedAttributeGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "attributeGroup")
    public JAXBElement<NamedAttributeGroup> createAttributeGroup(NamedAttributeGroup value) {
        return new JAXBElement<NamedAttributeGroup>(_AttributeGroup_QNAME, NamedAttributeGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "minLength")
    public JAXBElement<NumFacet> createMinLength(NumFacet value) {
        return new JAXBElement<NumFacet>(_MinLength_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoFixedFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "enumeration")
    public JAXBElement<NoFixedFacet> createEnumeration(NoFixedFacet value) {
        return new JAXBElement<NoFixedFacet>(_Enumeration_QNAME, NoFixedFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NamedGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "group")
    public JAXBElement<NamedGroup> createGroup(NamedGroup value) {
        return new JAXBElement<NamedGroup>(_Group_QNAME, NamedGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopLevelElement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "element")
    public JAXBElement<TopLevelElement> createElement(TopLevelElement value) {
        return new JAXBElement<TopLevelElement>(_Element_QNAME, TopLevelElement.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferSessionResponseData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "TransferSessionResponseData")
    public JAXBElement<TransferSessionResponseData> createTransferSessionResponseData(TransferSessionResponseData value) {
        return new JAXBElement<TransferSessionResponseData>(_TransferSessionResponseData_QNAME, TransferSessionResponseData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "maxInclusive")
    public JAXBElement<Facet> createMaxInclusive(Facet value) {
        return new JAXBElement<Facet>(_MaxInclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "minExclusive")
    public JAXBElement<Facet> createMinExclusive(Facet value) {
        return new JAXBElement<Facet>(_MinExclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Wildcard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "anyAttribute")
    public JAXBElement<Wildcard> createAnyAttribute(Wildcard value) {
        return new JAXBElement<Wildcard>(_AnyAttribute_QNAME, Wildcard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link APIUnhandledServerFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "APIUnhandledServerFault")
    public JAXBElement<APIUnhandledServerFault> createAPIUnhandledServerFault(APIUnhandledServerFault value) {
        return new JAXBElement<APIUnhandledServerFault>(_APIUnhandledServerFault_QNAME, APIUnhandledServerFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Facet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "maxExclusive")
    public JAXBElement<Facet> createMaxExclusive(Facet value) {
        return new JAXBElement<Facet>(_MaxExclusive_QNAME, Facet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopLevelSimpleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "simpleType")
    public JAXBElement<TopLevelSimpleType> createSimpleType(TopLevelSimpleType value) {
        return new JAXBElement<TopLevelSimpleType>(_SimpleType_QNAME, TopLevelSimpleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link All }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "all")
    public JAXBElement<All> createAll(All value) {
        return new JAXBElement<All>(_All_QNAME, All.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", name = "ValidationResult")
    public JAXBElement<ValidationResult> createValidationResult(ValidationResult value) {
        return new JAXBElement<ValidationResult>(_ValidationResult_QNAME, ValidationResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link APICriticalFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "APICriticalFault")
    public JAXBElement<APICriticalFault> createAPICriticalFault(APICriticalFault value) {
        return new JAXBElement<APICriticalFault>(_APICriticalFault_QNAME, APICriticalFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopLevelComplexType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "complexType")
    public JAXBElement<TopLevelComplexType> createComplexType(TopLevelComplexType value) {
        return new JAXBElement<TopLevelComplexType>(_ComplexType_QNAME, TopLevelComplexType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link APIValidationFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "APIValidationFault")
    public JAXBElement<APIValidationFault> createAPIValidationFault(APIValidationFault value) {
        return new JAXBElement<APIValidationFault>(_APIValidationFault_QNAME, APIValidationFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link APISecurityFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "APISecurityFault")
    public JAXBElement<APISecurityFault> createAPISecurityFault(APISecurityFault value) {
        return new JAXBElement<APISecurityFault>(_APISecurityFault_QNAME, APISecurityFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SerializableDictionaryOfstringstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dictionary")
    public JAXBElement<SerializableDictionaryOfstringstring> createDictionary(SerializableDictionaryOfstringstring value) {
        return new JAXBElement<SerializableDictionaryOfstringstring>(_Dictionary_QNAME, SerializableDictionaryOfstringstring.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "maxLength")
    public JAXBElement<NumFacet> createMaxLength(NumFacet value) {
        return new JAXBElement<NumFacet>(_MaxLength_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExplicitGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "sequence")
    public JAXBElement<ExplicitGroup> createSequence(ExplicitGroup value) {
        return new JAXBElement<ExplicitGroup>(_Sequence_QNAME, ExplicitGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link APIFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "APIFault")
    public JAXBElement<APIFault> createAPIFault(APIFault value) {
        return new JAXBElement<APIFault>(_APIFault_QNAME, APIFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link APIGeneralFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "APIGeneralFault")
    public JAXBElement<APIGeneralFault> createAPIGeneralFault(APIGeneralFault value) {
        return new JAXBElement<APIGeneralFault>(_APIGeneralFault_QNAME, APIGeneralFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Keybase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "unique")
    public JAXBElement<Keybase> createUnique(Keybase value) {
        return new JAXBElement<Keybase>(_Unique_QNAME, Keybase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices", name = "EnableExceptionStackTrace")
    public JAXBElement<Boolean> createEnableExceptionStackTrace(Boolean value) {
        return new JAXBElement<Boolean>(_EnableExceptionStackTrace_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogonRequestData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "LogonRequestData")
    public JAXBElement<LogonRequestData> createLogonRequestData(LogonRequestData value) {
        return new JAXBElement<LogonRequestData>(_LogonRequestData_QNAME, LogonRequestData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfValidationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", name = "ArrayOfValidationResult")
    public JAXBElement<ArrayOfValidationResult> createArrayOfValidationResult(ArrayOfValidationResult value) {
        return new JAXBElement<ArrayOfValidationResult>(_ArrayOfValidationResult_QNAME, ArrayOfValidationResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "TokenRequest")
    public JAXBElement<TokenRequest> createTokenRequest(TokenRequest value) {
        return new JAXBElement<TokenRequest>(_TokenRequest_QNAME, TokenRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopLevelAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "attribute")
    public JAXBElement<TopLevelAttribute> createAttribute(TopLevelAttribute value) {
        return new JAXBElement<TopLevelAttribute>(_Attribute_QNAME, TopLevelAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices", name = "ContractVersion")
    public JAXBElement<Integer> createContractVersion(Integer value) {
        return new JAXBElement<Integer>(_ContractVersion_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link APIWarningFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "APIWarningFault")
    public JAXBElement<APIWarningFault> createAPIWarningFault(APIWarningFault value) {
        return new JAXBElement<APIWarningFault>(_APIWarningFault_QNAME, APIWarningFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Keybase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "key")
    public JAXBElement<Keybase> createKey(Keybase value) {
        return new JAXBElement<Keybase>(_Key_QNAME, Keybase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices", name = "Signature")
    public JAXBElement<String> createSignature(String value) {
        return new JAXBElement<String>(_Signature_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "length")
    public JAXBElement<NumFacet> createLength(NumFacet value) {
        return new JAXBElement<NumFacet>(_Length_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NumFacet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "fractionDigits")
    public JAXBElement<NumFacet> createFractionDigits(NumFacet value) {
        return new JAXBElement<NumFacet>(_FractionDigits_QNAME, NumFacet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices", name = "MessageContractVersion")
    public JAXBElement<String> createMessageContractVersion(String value) {
        return new JAXBElement<String>(_MessageContractVersion_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChannelType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations", name = "ChannelType")
    public JAXBElement<ChannelType> createChannelType(ChannelType value) {
        return new JAXBElement<ChannelType>(_ChannelType_QNAME, ChannelType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExplicitGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "choice")
    public JAXBElement<ExplicitGroup> createChoice(ExplicitGroup value) {
        return new JAXBElement<ExplicitGroup>(_Choice_QNAME, ExplicitGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WhoAmIResponseData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "WhoAmIResponseData")
    public JAXBElement<WhoAmIResponseData> createWhoAmIResponseData(WhoAmIResponseData value) {
        return new JAXBElement<WhoAmIResponseData>(_WhoAmIResponseData_QNAME, WhoAmIResponseData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogonRequestData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", name = "logonRequestData", scope = ChangePasswordRequest.class)
    public JAXBElement<LogonRequestData> createChangePasswordRequestLogonRequestData(LogonRequestData value) {
        return new JAXBElement<LogonRequestData>(_ChangePasswordRequestLogonRequestData_QNAME, LogonRequestData.class, ChangePasswordRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", name = "newPassword", scope = ChangePasswordRequest.class)
    public JAXBElement<String> createChangePasswordRequestNewPassword(String value) {
        return new JAXBElement<String>(_ChangePasswordRequestNewPassword_QNAME, String.class, ChangePasswordRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalElement }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "element", scope = Group.class)
    public JAXBElement<LocalElement> createGroupElement(LocalElement value) {
        return new JAXBElement<LocalElement>(_Element_QNAME, LocalElement.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupRef }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2001/XMLSchema", name = "group", scope = Group.class)
    public JAXBElement<GroupRef> createGroupGroup(GroupRef value) {
        return new JAXBElement<GroupRef>(_Group_QNAME, GroupRef.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogonRequestData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", name = "logonRequestData", scope = LogonRequest.class)
    public JAXBElement<LogonRequestData> createLogonRequestLogonRequestData(LogonRequestData value) {
        return new JAXBElement<LogonRequestData>(_ChangePasswordRequestLogonRequestData_QNAME, LogonRequestData.class, LogonRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", name = "tokenRequest", scope = TransferSessionRequest.class)
    public JAXBElement<TokenRequest> createTransferSessionRequestTokenRequest(TokenRequest value) {
        return new JAXBElement<TokenRequest>(_TransferSessionRequestTokenRequest_QNAME, TokenRequest.class, TransferSessionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SerializableDictionaryOfstringstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", name = "Annotations", scope = ValidationResult.class)
    public JAXBElement<SerializableDictionaryOfstringstring> createValidationResultAnnotations(SerializableDictionaryOfstringstring value) {
        return new JAXBElement<SerializableDictionaryOfstringstring>(_ValidationResultAnnotations_QNAME, SerializableDictionaryOfstringstring.class, ValidationResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", name = "Key", scope = ValidationResult.class)
    public JAXBElement<String> createValidationResultKey(String value) {
        return new JAXBElement<String>(_ValidationResultKey_QNAME, String.class, ValidationResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.navitaire.com/Ncl/Validation/ValidationResult", name = "FailedValidationDescription", scope = ValidationResult.class)
    public JAXBElement<String> createValidationResultFailedValidationDescription(String value) {
        return new JAXBElement<String>(_ValidationResultFailedValidationDescription_QNAME, String.class, ValidationResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "Signature", scope = TransferSessionResponseData.class)
    public JAXBElement<String> createTransferSessionResponseDataSignature(String value) {
        return new JAXBElement<String>(_TransferSessionResponseDataSignature_QNAME, String.class, TransferSessionResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "ErrorCode", scope = APIFault.class)
    public JAXBElement<String> createAPIFaultErrorCode(String value) {
        return new JAXBElement<String>(_APIFaultErrorCode_QNAME, String.class, APIFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "ErrorType", scope = APIFault.class)
    public JAXBElement<String> createAPIFaultErrorType(String value) {
        return new JAXBElement<String>(_APIFaultErrorType_QNAME, String.class, APIFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "Message", scope = APIFault.class)
    public JAXBElement<String> createAPIFaultMessage(String value) {
        return new JAXBElement<String>(_APIFaultMessage_QNAME, String.class, APIFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "StackTrace", scope = APIFault.class)
    public JAXBElement<String> createAPIFaultStackTrace(String value) {
        return new JAXBElement<String>(_APIFaultStackTrace_QNAME, String.class, APIFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "DomainCode", scope = LogonRequestData.class)
    public JAXBElement<String> createLogonRequestDataDomainCode(String value) {
        return new JAXBElement<String>(_LogonRequestDataDomainCode_QNAME, String.class, LogonRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "ClientName", scope = LogonRequestData.class)
    public JAXBElement<String> createLogonRequestDataClientName(String value) {
        return new JAXBElement<String>(_LogonRequestDataClientName_QNAME, String.class, LogonRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "Password", scope = LogonRequestData.class)
    public JAXBElement<String> createLogonRequestDataPassword(String value) {
        return new JAXBElement<String>(_LogonRequestDataPassword_QNAME, String.class, LogonRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "RoleCode", scope = LogonRequestData.class)
    public JAXBElement<String> createLogonRequestDataRoleCode(String value) {
        return new JAXBElement<String>(_LogonRequestDataRoleCode_QNAME, String.class, LogonRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "AgentName", scope = LogonRequestData.class)
    public JAXBElement<String> createLogonRequestDataAgentName(String value) {
        return new JAXBElement<String>(_LogonRequestDataAgentName_QNAME, String.class, LogonRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "TerminalInfo", scope = LogonRequestData.class)
    public JAXBElement<String> createLogonRequestDataTerminalInfo(String value) {
        return new JAXBElement<String>(_LogonRequestDataTerminalInfo_QNAME, String.class, LogonRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "LocationCode", scope = LogonRequestData.class)
    public JAXBElement<String> createLogonRequestDataLocationCode(String value) {
        return new JAXBElement<String>(_LogonRequestDataLocationCode_QNAME, String.class, LogonRequestData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices", name = "Signature", scope = LogonResponse.class)
    public JAXBElement<String> createLogonResponseSignature(String value) {
        return new JAXBElement<String>(_Signature_QNAME, String.class, LogonResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "Token", scope = TokenRequest.class)
    public JAXBElement<String> createTokenRequestToken(String value) {
        return new JAXBElement<String>(_TokenRequestToken_QNAME, String.class, TokenRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "TerminalInfo", scope = TokenRequest.class)
    public JAXBElement<String> createTokenRequestTerminalInfo(String value) {
        return new JAXBElement<String>(_LogonRequestDataTerminalInfo_QNAME, String.class, TokenRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferSessionResponseData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", name = "TransferSessionResponseData", scope = TransferSessionResponse.class)
    public JAXBElement<TransferSessionResponseData> createTransferSessionResponseTransferSessionResponseData(TransferSessionResponseData value) {
        return new JAXBElement<TransferSessionResponseData>(_TransferSessionResponseTransferSessionResponseData_QNAME, TransferSessionResponseData.class, TransferSessionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfValidationResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/FaultContracts", name = "ValidationResults", scope = APIValidationFault.class)
    public JAXBElement<ArrayOfValidationResult> createAPIValidationFaultValidationResults(ArrayOfValidationResult value) {
        return new JAXBElement<ArrayOfValidationResult>(_APIValidationFaultValidationResults_QNAME, ArrayOfValidationResult.class, APIValidationFault.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WhoAmIResponseData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", name = "WhoAmIResponseData", scope = WhoAmIResponse.class)
    public JAXBElement<WhoAmIResponseData> createWhoAmIResponseWhoAmIResponseData(WhoAmIResponseData value) {
        return new JAXBElement<WhoAmIResponseData>(_WhoAmIResponseWhoAmIResponseData_QNAME, WhoAmIResponseData.class, WhoAmIResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "DomainCode", scope = WhoAmIResponseData.class)
    public JAXBElement<String> createWhoAmIResponseDataDomainCode(String value) {
        return new JAXBElement<String>(_LogonRequestDataDomainCode_QNAME, String.class, WhoAmIResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "RoleCode", scope = WhoAmIResponseData.class)
    public JAXBElement<String> createWhoAmIResponseDataRoleCode(String value) {
        return new JAXBElement<String>(_LogonRequestDataRoleCode_QNAME, String.class, WhoAmIResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "AgentName", scope = WhoAmIResponseData.class)
    public JAXBElement<String> createWhoAmIResponseDataAgentName(String value) {
        return new JAXBElement<String>(_LogonRequestDataAgentName_QNAME, String.class, WhoAmIResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "OrganizationCode", scope = WhoAmIResponseData.class)
    public JAXBElement<String> createWhoAmIResponseDataOrganizationCode(String value) {
        return new JAXBElement<String>(_WhoAmIResponseDataOrganizationCode_QNAME, String.class, WhoAmIResponseData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Session", name = "LocationCode", scope = WhoAmIResponseData.class)
    public JAXBElement<String> createWhoAmIResponseDataLocationCode(String value) {
        return new JAXBElement<String>(_LogonRequestDataLocationCode_QNAME, String.class, WhoAmIResponseData.class, value);
    }

}
