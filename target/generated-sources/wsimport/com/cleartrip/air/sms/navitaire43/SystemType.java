
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SystemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SystemType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="WinRez"/>
 *     &lt;enumeration value="FareManager"/>
 *     &lt;enumeration value="ScheduleManager"/>
 *     &lt;enumeration value="WinManager"/>
 *     &lt;enumeration value="ConsoleRez"/>
 *     &lt;enumeration value="WebRez"/>
 *     &lt;enumeration value="WebServicesAPI"/>
 *     &lt;enumeration value="WebServicesESC"/>
 *     &lt;enumeration value="InternalService"/>
 *     &lt;enumeration value="WebReporting"/>
 *     &lt;enumeration value="TaxAndFeeManager"/>
 *     &lt;enumeration value="DCS"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SystemType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum SystemType {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("WinRez")
    WIN_REZ("WinRez"),
    @XmlEnumValue("FareManager")
    FARE_MANAGER("FareManager"),
    @XmlEnumValue("ScheduleManager")
    SCHEDULE_MANAGER("ScheduleManager"),
    @XmlEnumValue("WinManager")
    WIN_MANAGER("WinManager"),
    @XmlEnumValue("ConsoleRez")
    CONSOLE_REZ("ConsoleRez"),
    @XmlEnumValue("WebRez")
    WEB_REZ("WebRez"),
    @XmlEnumValue("WebServicesAPI")
    WEB_SERVICES_API("WebServicesAPI"),
    @XmlEnumValue("WebServicesESC")
    WEB_SERVICES_ESC("WebServicesESC"),
    @XmlEnumValue("InternalService")
    INTERNAL_SERVICE("InternalService"),
    @XmlEnumValue("WebReporting")
    WEB_REPORTING("WebReporting"),
    @XmlEnumValue("TaxAndFeeManager")
    TAX_AND_FEE_MANAGER("TaxAndFeeManager"),
    DCS("DCS"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    SystemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SystemType fromValue(String v) {
        for (SystemType c: SystemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
