
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NestType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="Net"/>
 *     &lt;enumeration value="Serial"/>
 *     &lt;enumeration value="OneBooking"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "NestType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum NestType {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Net")
    NET("Net"),
    @XmlEnumValue("Serial")
    SERIAL("Serial"),
    @XmlEnumValue("OneBooking")
    ONE_BOOKING("OneBooking"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    NestType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NestType fromValue(String v) {
        for (NestType c: NestType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
