
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VoucherTransactionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VoucherTransactionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Debit"/>
 *     &lt;enumeration value="Credit"/>
 *     &lt;enumeration value="Void"/>
 *     &lt;enumeration value="Reinstate"/>
 *     &lt;enumeration value="Adjustment"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VoucherTransactionType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum VoucherTransactionType {

    @XmlEnumValue("Debit")
    DEBIT("Debit"),
    @XmlEnumValue("Credit")
    CREDIT("Credit"),
    @XmlEnumValue("Void")
    VOID("Void"),
    @XmlEnumValue("Reinstate")
    REINSTATE("Reinstate"),
    @XmlEnumValue("Adjustment")
    ADJUSTMENT("Adjustment"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    VoucherTransactionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VoucherTransactionType fromValue(String v) {
        for (VoucherTransactionType c: VoucherTransactionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
