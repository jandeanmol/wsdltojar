
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FareStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FareStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Default"/>
 *     &lt;enumeration value="SameDayStandBy"/>
 *     &lt;enumeration value="FareOverrideConfirming"/>
 *     &lt;enumeration value="FareOverrideConfirmed"/>
 *     &lt;enumeration value="PublishedFareOverrideConfirming"/>
 *     &lt;enumeration value="PublishedFareOverrideConfirmed"/>
 *     &lt;enumeration value="Unmapped"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FareStatus", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations")
@XmlEnum
public enum FareStatus {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("SameDayStandBy")
    SAME_DAY_STAND_BY("SameDayStandBy"),
    @XmlEnumValue("FareOverrideConfirming")
    FARE_OVERRIDE_CONFIRMING("FareOverrideConfirming"),
    @XmlEnumValue("FareOverrideConfirmed")
    FARE_OVERRIDE_CONFIRMED("FareOverrideConfirmed"),
    @XmlEnumValue("PublishedFareOverrideConfirming")
    PUBLISHED_FARE_OVERRIDE_CONFIRMING("PublishedFareOverrideConfirming"),
    @XmlEnumValue("PublishedFareOverrideConfirmed")
    PUBLISHED_FARE_OVERRIDE_CONFIRMED("PublishedFareOverrideConfirmed"),
    @XmlEnumValue("Unmapped")
    UNMAPPED("Unmapped");
    private final String value;

    FareStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FareStatus fromValue(String v) {
        for (FareStatus c: FareStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
