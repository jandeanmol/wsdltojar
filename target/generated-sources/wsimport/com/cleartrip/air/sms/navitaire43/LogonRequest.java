
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="logonRequestData" type="{http://schemas.navitaire.com/WebServices/DataContracts/Session}LogonRequestData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "logonRequestData"
})
@XmlRootElement(name = "LogonRequest", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService")
public class LogonRequest {

    @XmlElementRef(name = "logonRequestData", namespace = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService", type = JAXBElement.class, required = false)
    protected JAXBElement<LogonRequestData> logonRequestData;

    /**
     * Gets the value of the logonRequestData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LogonRequestData }{@code >}
     *     
     */
    public JAXBElement<LogonRequestData> getLogonRequestData() {
        return logonRequestData;
    }

    /**
     * Sets the value of the logonRequestData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LogonRequestData }{@code >}
     *     
     */
    public void setLogonRequestData(JAXBElement<LogonRequestData> value) {
        this.logonRequestData = value;
    }

}
