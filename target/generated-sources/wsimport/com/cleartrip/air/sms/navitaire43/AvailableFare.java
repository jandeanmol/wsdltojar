
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AvailableFare complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AvailableFare">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Booking}Fare">
 *       &lt;sequence>
 *         &lt;element name="AvailableCount" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}ClassStatus" minOccurs="0"/>
 *         &lt;element name="SSRIndexes" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfshort" minOccurs="0"/>
 *         &lt;element name="ServiceBundleSetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceBundleOfferIndexes" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfshort" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AvailableFare", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "availableCount",
    "status",
    "ssrIndexes",
    "serviceBundleSetCode",
    "serviceBundleOfferIndexes"
})
public class AvailableFare
    extends Fare
{

    @XmlElement(name = "AvailableCount")
    protected Short availableCount;
    @XmlElement(name = "Status")
    protected ClassStatus status;
    @XmlElementRef(name = "SSRIndexes", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfshort> ssrIndexes;
    @XmlElementRef(name = "ServiceBundleSetCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceBundleSetCode;
    @XmlElementRef(name = "ServiceBundleOfferIndexes", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfshort> serviceBundleOfferIndexes;

    /**
     * Gets the value of the availableCount property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAvailableCount() {
        return availableCount;
    }

    /**
     * Sets the value of the availableCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAvailableCount(Short value) {
        this.availableCount = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link ClassStatus }
     *     
     */
    public ClassStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassStatus }
     *     
     */
    public void setStatus(ClassStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the ssrIndexes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfshort }{@code >}
     *     
     */
    public JAXBElement<ArrayOfshort> getSSRIndexes() {
        return ssrIndexes;
    }

    /**
     * Sets the value of the ssrIndexes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfshort }{@code >}
     *     
     */
    public void setSSRIndexes(JAXBElement<ArrayOfshort> value) {
        this.ssrIndexes = value;
    }

    /**
     * Gets the value of the serviceBundleSetCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceBundleSetCode() {
        return serviceBundleSetCode;
    }

    /**
     * Sets the value of the serviceBundleSetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceBundleSetCode(JAXBElement<String> value) {
        this.serviceBundleSetCode = value;
    }

    /**
     * Gets the value of the serviceBundleOfferIndexes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfshort }{@code >}
     *     
     */
    public JAXBElement<ArrayOfshort> getServiceBundleOfferIndexes() {
        return serviceBundleOfferIndexes;
    }

    /**
     * Sets the value of the serviceBundleOfferIndexes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfshort }{@code >}
     *     
     */
    public void setServiceBundleOfferIndexes(JAXBElement<ArrayOfshort> value) {
        this.serviceBundleOfferIndexes = value;
    }

}
