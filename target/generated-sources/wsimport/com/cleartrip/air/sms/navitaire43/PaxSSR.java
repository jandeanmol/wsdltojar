
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaxSSR complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaxSSR">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.navitaire.com/WebServices/DataContracts/Common}StateMessage">
 *       &lt;sequence>
 *         &lt;element name="ActionStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PassengerNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="SSRCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSRNumber" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="SSRDetail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSRValue" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="IsInServiceBundle" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxSSR", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", propOrder = {
    "actionStatusCode",
    "arrivalStation",
    "departureStation",
    "passengerNumber",
    "ssrCode",
    "ssrNumber",
    "ssrDetail",
    "feeCode",
    "note",
    "ssrValue",
    "isInServiceBundle"
})
public class PaxSSR
    extends StateMessage
{

    @XmlElementRef(name = "ActionStatusCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actionStatusCode;
    @XmlElementRef(name = "ArrivalStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> arrivalStation;
    @XmlElementRef(name = "DepartureStation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departureStation;
    @XmlElement(name = "PassengerNumber")
    protected Short passengerNumber;
    @XmlElementRef(name = "SSRCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ssrCode;
    @XmlElement(name = "SSRNumber")
    protected Short ssrNumber;
    @XmlElementRef(name = "SSRDetail", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ssrDetail;
    @XmlElementRef(name = "FeeCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> feeCode;
    @XmlElementRef(name = "Note", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Booking", type = JAXBElement.class, required = false)
    protected JAXBElement<String> note;
    @XmlElement(name = "SSRValue")
    protected Short ssrValue;
    @XmlElement(name = "IsInServiceBundle")
    protected Boolean isInServiceBundle;

    /**
     * Gets the value of the actionStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActionStatusCode() {
        return actionStatusCode;
    }

    /**
     * Sets the value of the actionStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActionStatusCode(JAXBElement<String> value) {
        this.actionStatusCode = value;
    }

    /**
     * Gets the value of the arrivalStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrivalStation() {
        return arrivalStation;
    }

    /**
     * Sets the value of the arrivalStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrivalStation(JAXBElement<String> value) {
        this.arrivalStation = value;
    }

    /**
     * Gets the value of the departureStation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartureStation() {
        return departureStation;
    }

    /**
     * Sets the value of the departureStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartureStation(JAXBElement<String> value) {
        this.departureStation = value;
    }

    /**
     * Gets the value of the passengerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPassengerNumber() {
        return passengerNumber;
    }

    /**
     * Sets the value of the passengerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPassengerNumber(Short value) {
        this.passengerNumber = value;
    }

    /**
     * Gets the value of the ssrCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSSRCode() {
        return ssrCode;
    }

    /**
     * Sets the value of the ssrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSSRCode(JAXBElement<String> value) {
        this.ssrCode = value;
    }

    /**
     * Gets the value of the ssrNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSSRNumber() {
        return ssrNumber;
    }

    /**
     * Sets the value of the ssrNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSSRNumber(Short value) {
        this.ssrNumber = value;
    }

    /**
     * Gets the value of the ssrDetail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSSRDetail() {
        return ssrDetail;
    }

    /**
     * Sets the value of the ssrDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSSRDetail(JAXBElement<String> value) {
        this.ssrDetail = value;
    }

    /**
     * Gets the value of the feeCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFeeCode() {
        return feeCode;
    }

    /**
     * Sets the value of the feeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFeeCode(JAXBElement<String> value) {
        this.feeCode = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNote(JAXBElement<String> value) {
        this.note = value;
    }

    /**
     * Gets the value of the ssrValue property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSSRValue() {
        return ssrValue;
    }

    /**
     * Sets the value of the ssrValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSSRValue(Short value) {
        this.ssrValue = value;
    }

    /**
     * Gets the value of the isInServiceBundle property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsInServiceBundle() {
        return isInServiceBundle;
    }

    /**
     * Sets the value of the isInServiceBundle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsInServiceBundle(Boolean value) {
        this.isInServiceBundle = value;
    }

}
