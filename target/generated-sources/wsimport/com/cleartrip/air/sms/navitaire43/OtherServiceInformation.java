
package com.cleartrip.air.sms.navitaire43;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OtherServiceInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OtherServiceInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OsiSeverity" type="{http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations}OSISeverity" minOccurs="0"/>
 *         &lt;element name="OSITypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherServiceInformation", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", propOrder = {
    "text",
    "osiSeverity",
    "osiTypeCode",
    "subType"
})
public class OtherServiceInformation {

    @XmlElementRef(name = "Text", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> text;
    @XmlElement(name = "OsiSeverity")
    protected OSISeverity osiSeverity;
    @XmlElementRef(name = "OSITypeCode", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> osiTypeCode;
    @XmlElementRef(name = "SubType", namespace = "http://schemas.navitaire.com/WebServices/DataContracts/Common", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subType;

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setText(JAXBElement<String> value) {
        this.text = value;
    }

    /**
     * Gets the value of the osiSeverity property.
     * 
     * @return
     *     possible object is
     *     {@link OSISeverity }
     *     
     */
    public OSISeverity getOsiSeverity() {
        return osiSeverity;
    }

    /**
     * Sets the value of the osiSeverity property.
     * 
     * @param value
     *     allowed object is
     *     {@link OSISeverity }
     *     
     */
    public void setOsiSeverity(OSISeverity value) {
        this.osiSeverity = value;
    }

    /**
     * Gets the value of the osiTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOSITypeCode() {
        return osiTypeCode;
    }

    /**
     * Sets the value of the osiTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOSITypeCode(JAXBElement<String> value) {
        this.osiTypeCode = value;
    }

    /**
     * Gets the value of the subType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubType() {
        return subType;
    }

    /**
     * Sets the value of the subType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubType(JAXBElement<String> value) {
        this.subType = value;
    }

}
